

跟我了解的农村是一样的。讲一个故事，不信就是我编的，请勿对号入座，权听乐  
  
背景是某村门口有条河，多年无桥，附近村民交通不便，枯水期踩着石头过河，丰水期坐农用三轮车或者淌水。  
后来政府改善民生，推村村通公路，把桥修起来了，双车道宽阔的水泥桥。因为修桥，河道变窄，遇上大雨会有少量水溢出河道，流入旁边的玉米地。村民隧跟政府要赔偿，漫天要价，沟通不下来，政府只能把桥炸了，大家重回淌水过河时代。  
冬天，附近村民包括半大小孩最喜欢做的事就是去河边，把河道挖深，等着路过的车子压碎薄冰陷进去，然后收费推车。

分享一个我们这边的故事，和宁夏瓜农如出一辙:

《因为一户菇农不剪香菇把儿，全县香菇烂在地里》

————————————

我家乡盛产香菇，香菇这玩意大家都吃过

——红伞伞，白杆杆，吃完一起躺板板！

吃香菇都是吃香菇的伞伞，没人会闲的蛋疼去吃香菇杆杆。

香菇杆杆我们俗语叫香菇“把儿”，没味道不说，又嚼不烂。

所以香菇商人在收香菇的时候会要求把香菇把儿剪了——这关乎到香菇的品质口感。

十年前，某县招商引资，找来了一批香菇收购商。

客商对当地香菇的品质很看好，计划收了这批香菇加工后销往欧洲。

客商愿意出高出当时市场价格30%的报价收购香菇，而且需求量很大，有多少要多少。

唯一的要求就是——“香菇采摘时，必须把香菇杆杆剪了！”

原因很简单，消费者吃香菇追求口感，谁也不吃香菇把儿。欧洲的消费者尤其挑剔。

客商高价收购香菇的消息一传开，全县轰动，许多菇农连忙采摘香菇来买。

前两天还好，收来的香菇都是剪了把儿的，本地香菇的品质也好。客商和农民都高兴。

到了第三天，意料之中的意外情况还是发生了——在收到的香菇中，有少部分农民保留了一小截香菇把儿。

这让客商很不满，再三强调“不收带把儿香菇”

但是毕竟农民大老远的把香菇都运来了，也不好直接把人赶回去。客商还是硬着头皮收了，也暗自捏了一把汗——希望国外客户不要投诉他。

可是到了第五天，更糟糕的情况发生了，来了一户农民（我们叫他刁民，他还会在后面的故事里反复出现，算是主角之一了）

刁民拉了几百斤香菇过来，全是带把儿的。而且把儿非常的长——完全是一点没剪！

这下客商就不能接受了，留着么长的香菇把儿让消费者怎么吃。

客商断然拒绝收购该刁民的香菇，刁民不服，说:“我大老远把香菇拉到你这里，你不收也得收！”

客商坚持不收，说:“带把儿的香菇口感不好，要收只能打对折！”

五六个农民冲上前来与他理论。客商手下的员工也冲上来对峙。

突然那个刁民大喊一声:“外地人欺负咱们村的人，把我们的香菇骗过来坐地杀价！”

一下子几十个农民就围了上来，客商怕了，只能被迫高价收了他不剪把儿的香菇。

原本以为就一个不讲理的农民，可没想到第二天所有农民送来的香菇都是不剪把儿的！

第二天客商又被迫收了两车香菇，当晚就带着人跑路了。

县里招商的干部看着客商跑了，赶紧追过去问情况。

客商说:我加价30%收香菇，要求他们剪把儿。他们还要贪图香菇把儿那点重量，不剪香菇把儿，还威胁我必须高价收购。

招商干部一调查，就了解了全部过程:

1.香菇把儿不剪影响口感，欧洲客户会投诉。

2.剪了香菇把儿，会减少香菇5%的重量。但是客商加价30%，农民肯定是大赚。

3.有的农民无比自私，既要按照剪把儿后的高价，又要保留香菇把儿多5%的重量。完全不考虑香菇口感问题。

4.香菇把儿必须采摘的时候当场就剪，客商拉回去再剪麻烦不说，还因晒干后香菇很脆，后期再剪香菇把儿会弄碎香菇。

县里让招商和农业的干部去搞宣传，去做调解，要求所有的农民必须剪香菇把儿。

可是没维持几天，那个不剪香菇把儿的刁民又来了，还是继续强迫客商买他的带把儿香菇。

这次真把客商吓跑了，再也不来了。

全县农民的香菇都卖不出去！

没办法，眼见这几天天气不好，必须要收香菇了。

勤快点的农民都把香菇收了晒干，但是还是抱着贪小便宜的心里，保留了香菇把儿。希望上称的时候能多称几斤。

但是客商不来收，多称一万斤又能怎样呢？

县里对此很着急，赶紧联系其他客商。可是收香菇的商人有他们的生意圈，一听说这个故事都不来了。

最后没办法，有个人提议——要不还是让张三他们来收香菇，张三有办法，农民不敢不剪把儿！

这个张三是谁？凭什么面对张三，农民就不敢不剪香菇把儿了？

其实，很简单，张三这个人表面上是本地商人，实际上是个流氓地头蛇。

几年前因为生意纠纷把人打残废了，坐牢到现在才释放。

第二天，张三联系了本地的其他“商人”（流氓兼职商人），立起牌子，以市场价五折的价格收购香菇——必须剪香菇把儿，不然把你的香菇倒沟里！

市场价五折实在太低了，农民都不愿意卖，宁愿香菇烂在家里。毕竟，此前客商是以市场价的130%收购的。

客商加价收购的时候他们都还不满足，还要偷偷摸摸用不剪香菇把儿的来增加重量。

如今张三按照市场价格的五折收购，还要求必须剪香菇把儿——农民当然不愿意卖。

可是客商被吓跑了，本地商人和张三是一伙的，现在没人收香菇了。

又等了几天，农民实在受不了了，只能以五折的价格卖香菇给张三。

毕竟，马上孩子就要开学了，很多农民都等着钱交学费！

绝大多数的农民都老老实实地把香菇把儿剪了，按照五折的价格出售给张三了。

可是还是那个之前坚持不剪香菇把儿的刁民，他又来了，带着几百斤不剪把儿的香菇又来了！

张三可不惯着他，麻袋里的香菇翻出来一看，全是不剪把儿的。张三直接连三轮车带香菇一起给他倒进臭水沟了！

刁民找张三理论，张三手下几个小弟冲上来，把刁民摁在地上狠狠地锤了一顿。

不知道是因为之前刁民惹跑了客商让大家亏损，还是因为大家惧怕张三是地痞。

这一次无论刁民怎么叫唤，农民们都不愿意帮刁民了！属实狼来了的故事！

短短几天时间，张三收了三十万斤的香菇，全是剪把儿的，品质非常好。

张三的朋友们，也就是其他的本地“商人”也收了不少香菇。总共加起来有上百万斤。

可是问题来了，张三没有香菇加工厂，又没有欧洲销售渠道，他收那么多香菇干什么？

当天晚上，张三请农业部门的领导吃饭，也把之前被吓跑的客商请了过来。说是替全县农民向客商赔个不是。

酒桌上，张三跟客商表明:“香菇我已经收了一百万斤了，都是剪了把儿的，品质很好。现在我按照你之前的收购价格，也就是市场价再上浮30%买给你”

客商一听，可高兴坏了:他本来就接受这个价格，这几天因为没收到香菇耽误了订单进度，现在正在着急呢，张三直接把香菇收购齐了。

张三还表示，以后不用客商亲自来这里了，客商要香菇直接找他，一直都是这个价格。保证质量！

————————————————

全局下来，所有人都赢了：

县招商完成了招商任务，

县农业完成了香菇销售任务，

客商不用每天都去和农民扯皮，

张三作为中介商发了发财！

唯独是农民亏了，本来没有中间商赚差价，市场价上浮30%厂家直收。

现在市场价打对折，如果不服气还要被张三揍！

真的是一个不剪香菇把儿的刁民害了全县农民吗？

难道那些一开始就偷偷保留一小截香菇把儿企图蒙混过关的农民没有责任吗？

难道那些第二天有样学样不剪香菇把儿的农民没有责任吗？

难道那些跟着刁民起哄赶跑客商的人没有责任吗？

————————————

多年后的某一天，我在酒桌上同时遇到了张三和那个不剪香菇把儿的刁民，刁民叫张三一声大哥。

不知道是不是之前被张三打怕了，甘愿做了小弟。还是刁民和张三之前就认识！

应该结局改一下，不剪把的那个农民是张三远房亲戚，原来当时是他和张三演的一出苦肉计

这让我想起了我们那边外地人来收黑芝麻的故事。老家湖北很多山地，适合种植黑芝麻，各家各户多少都会种些，有的家庭产量能有2-300斤。我记得2004年左右能卖10来元一斤。很多外地客商开小货车到村里来收购，有些人就动歪脑筋了，往里面掺铁砂，自从这一年以后，大家的黑芝麻只能便宜卖给本地的泼皮，由他们转手给外面的。图片就是铁砂，用吸铁石从河里吸上来的，很压秤。