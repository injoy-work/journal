# 核心公式

## 商业与社会

1.满意度=实际体验-期望。—满意度

2.组织效能=分工细化x合作高效。——分合协同

3.最终收益=初始本金x(1+收益率)^时间。一复利

4.创造力=知识基础x思维灵活性×动机。—创造力

5.时间价值=单位时间产出x有效工作时间。——时间价值

6权力影响力=强制力x说服力x合法化程度。—权力运作

7.机会成本=所放弃方案中价值最高选项的价值。—机会成本

8.消费决策=账户分类认知x价值感知偏差。——心理账户公式

9.网络影响力=内容价值x传播范围x互动程度。—网络影响力

10.品牌传播影响力=显性营销推广x隐性口碑塑造。—隐显并用

11.对方接收信息=自己表达信息x(1-信息遗漏率)。 一沟通漏斗

12.投资回报率=(投资收益-投资成本)+投资成本。—投资回报率

13.拓展潜力=市场规模×增长速度-进入壁垒×竞争强度。——市场拓展

14.沟通效率与效果=复杂信息简化能力x简单信息丰富度。—繁简互化

15.人际关系亲疏程度=互动频率x积极体验-消极体验。——人际关系亲疏

16.市场竞争力=产品差异化程度+成本优势-竞争对手实力。—市场竞争力

17.关系和谐度=尊重差异程度×共情理解深度x利益平衡能力。—和谐关系

18.招聘价值=岗位匹配度x能力潜力-招聘成本+员工留存率。——人才招聘

19.创意数量=知识储备量x思维发散程度x跨领域信息碰撞频率。——创意生成

20.历史发展方向=(各社会阶层群体的利益诉求x行动能力)。—历史发展合力

21.亲子关系亲密度=陪伴质量x有效沟通频率x尊重理解程度。—亲子关系亲密度

22.文化传承效果=文化载体丰富度x代际传播意愿x社会环境接纳度。—文化传承

23.员工绩效提升=关注与尊重投入度x反馈沟通频率x参与感营造强度。 一霍菜效应

24.沟通效果=结论先行明确度x论据分组逻辑性x上下层级支撑强度。 一金字塔原理表达

25.合作信任程度=过往合作成功次数x信息透明度＋利益一致性程度。—合作信任建立

26.营销效果预估-受众覆盖量x受众转化率-营销成本+预期品牌提升度。—营销方案

27.团队协作效率=成员能力互补程度x沟通顺畅度x团队目标一致性。—团队协作效率

28.社会变革潜力=对社会现状的批判性反思深度×大众意识觉醒程度。- 批判理论反思

29.社会公平程度=资源分配均衡度x机会均等程度x权利保障充分度。——社会公平程度

30.关系亲密度提升=给予对方价值x对方感知并回馈价值的可能性。—人际关系互惠原则
## 成长与认知

1.后果=事件x信念。—情绪ABC理论

2.稳健性=快速应变x慢速积累。- —快慢相济

3.，意境营造=实境描绘x虚境联想。 虚实相生

4.全面认知=正面洞察x反面反思。 正反互补

5.表达深度=显性明晰x隐性暗示。 -显隐互衬

6.节奏美感=动态活力x静态沉稳。 -动静相宜

7.学习深度=浅层涉猎x深层钻研。 -深浅交融

8.个人可信度=行为诚信x信念坚守。， -表里如一

9.学习吸收率：主动学习>被动学习。 -金字塔公式

10.成长动力=挑战程度-焦虑程度。 一心理舒适区公式

11.动机强度=任务难度x期望×价值。——动机强度公式

12.习惯形成=重复行为x暗示x奖赏。——习惯养成公式

13.能力认知偏差=实际能力-自我评估能力。—达克效应

14.情绪强度=刺激强度x认知评估。—普拉切克情绪理论

15.睡眠质量=深度睡眠时长+浅睡眼时长。——随眠周期公式

16.社交舒适度=热情投入亲和x冷静自持理性。—冷热均衡

17.布局合理性=密集部分充实×稀疏部分留白。、 -疏密有致

18.生活平衡感=紧张节奏效率x松弛节奏舒缓。 -张弛有度

19.团队领导力=刚性制度执行x柔性人文关怀。 软硬兼施

20.生活和谐度=动态活动活力x静态休憩宁静。 -动静相合

21.格局大小=视野广度x目标高度x利他程度。 格局拓展

22.个人魅力展现=才华展露精彩x锋芒内敛谦逊。 藏露适度

23.目标达成动力=高远目标引领x低近目标激励。 -高低相衬

24.社会适应能力=清流坚守原则x浊流灵活应对。 -清浊共处

25.信息传达效果=明示信息清晰x暗示信息巧妙。 -明暗互补

26.压力感受=外界压力源强度x心理承受力的倒数。—压力公式

27.知识体系完善度=纵向知识深化x横向知识关联。 一纵横拓展

28.审美体验感=浓烈元素表现力x淡雅元素调和度。一 -浓淡相宜

29.心态蜕变=逆境复原力×情绪超脱力×观念更新意愿。——心态进化

30.智慧值=知识内化率x经验反思度x思维创新频度。—智慧增长公式

## 哲学与思维

1.个体本质=自由选择x责任承担。——存在主义选择

2.洞察深度=现象解构力x规律抽象力x逻辑演绎力。——本质穿透

3.质变发生概率=量变积累程度x变化条件成熟度。 一量变与质变转化

4.本质显现程度=存在的展开过程x主体认知深度。—存在与本质关系

5.事物发展新状态=肯定因素x否定因素x否定之否定规律。——辩证发展

6.个体认知结构=原有认知图式x新信息同化x新信息顺应。——认知构建

7.心灵平静指数=欲望克制程度×无常接纳程度×内在精神富足。——心灵平静

8.自我实现程度=本真觉察深度×意志自由发挥x价值创造持续性，——自我实现

9.认知真理程度=怀疑精神强度x经验验证深度x理性推理严谨性。——认知真理

10.意义产生=结构元素间的关系(差异与对立)x系统的整体性。——结构主义分析

11.自我认知=他人对自己的态度(符号化)x个体对这些符号的解读。——符号互动论

12.时间体验质量=当下专注程度x过去意义赋予能力x未来愿景清浙度。—时间体验

13.存在意义感=个体独特性彰显x与世界关联性构建x生命历程反思深度。——存在意义

14.生命充盈感=存在意义构建强度x虚无体验转化度x当下存在专注度。—存在与虚无

15.系统和谐度-部分功能发挥度x整体目标契合度x部分间协同紧密度。—整体与部分

16.哲学思辦深度=问题挖掘深度x逻辑论证严密性×思想创新性。——哲学思辦深度公式

17.现象对本体的反映程度=现象的呈现完整性x认知方式的合理性。——现象与本体关联公式

18.矛盾化解成效=矛盾本质识别能力义对立统一转化能力×动态平衡维持能力。—矛盾辩证统一

19.人生价值达成度=理想目标清浙度x现实资源整合能力x实践行动持续性。——理想与现实对接

20.普遍性与特殊性的融合度=共性特征的提取程度×个性差异的保留程度。——普遍性与特殊性统一

21.事件发展轨迹的确定性=必然性趋势的主导性x偶然性因索的影响权重。—必然性与偶然性互动

22.目标达成的有效性=目的的明确性x手段的适宜性x手段对目的的支撑持续性。—目的与手段关联性

23.可能性向现实性转化概率=潜在可能性程度x现实条件具备度x主观努力程度。——可能性与现实性转化

24.超越幅度-舒适区突破频次x目标设定挑战性x心态调整弹性x成长反愦利用效率。——自我超越公式

25.存在明晰度=自我认知纯粹度-社会规训干扰度+存在意义叩问频率x存在体验沉浸深度。一存在本真探寻

26.事物发展逻辑自洽性=因果关系推导严密性x目的导向明确性×因果与目的的相互支撑强度。—因果与目的互构

27.系统稳定性与适应性综合指标=静态结构稳固性x动态变化响应速度×平衡调控机制有效性。 -静态与动态平衡 28，道德进步层级=道德原则内化深度×道德行为实践广度+道德困境反思深度-群体道德盲从度。——道德进步层级

29.社会和谐与个体发展指数=个体自由实现程度×社会规则遵循程度x规则弹性与自由边界契合度。—自由规则契合

30.整体洞察与局部理解深度=宏观规律把握精度x微观细节解析程度×宏观与微观的相互映射强度。—宏观微观关联

## 商业与社会

1.满意度=实际体验-期望。—满意度

2.组织效能=分工细化x合作高效。——分合协同

3，最终收益=初始本金x(1+收益率)^时间。一复利

4.创造力=知识基础x思维灵活性×动机。—创造力

5.时间价值=单位时间产出x有效工作时间。—时间价值

6权力影响力=强制力x说服力×合法化程度。—权力运作

7.机会成本=所放弃方案中价值最高选项的价值。—机会成本

8.消费决策=账户分类认知x价值感知偏差。——心理账户公式

9.网络影响力=内容价值×传播范围x互动程度。—网络影响力

10.品牌传播影响力=显性营销推广x隐性口碑塑造。—隐显并用

11.对方接收信息=自己表达信息x(1-信息遗漏率)。——沟通漏斗

12.投资回报率=(投资收益-投资成本)+投资成本。—投资回报率

13.拓展潜力=市场规模x增长速度-进入壁垒x竞争强度。——市场拓展

14.沟通效率与效果=复杂信息简化能力x简单信息丰富度。—繁简互化

15.人际关系亲琉程度=互动频率x积极体验-消极体验。——人际关系亲疏

16.市场竞争力=产品差异化程度+成本优势-竞争对手实力。—市场竞争力

17.关系和谐度=尊重差异程度x共情理解深度×利益平衡能力。——和谐关系

18.招聘价值=岗位匹配度x能力潜力-招聘成本+员工留存率。—人才招聘

19.创意数量=知识储备量x思维发散程度×跨领域信息碰撞频率。——创意生成

20.历史发展方向=(各社会阶层群体的利益诉求x行动能力)。—历史发展合力

21.亲子关系亲密度=陪伴质量x有效沟通频率x尊重理解程度。—亲子关系亲密度

22.文化传承效果=文化载体丰富度x代际传播意愿x社会环境接纳度。——文化传承

23.员工绩效提升=关注与尊重投入度x反馈沟通频率x参与感营造强度。—霍桑效应

24.沟通效果=结论先行明确度x论据分组逻辑性×上下层级支撑强度。—金字塔原理表达

25.合作信任程度=过往合作成功次数×信息透明度+利益一致性程度。—合作信任建立

26.营销效果预估=受众覆盖量×受众转化率-营销成本+预期品牌提升度。—营销方案

27.团队协作效率=成员能力互补程度×沟通顺畅度×团队目标一致性。——团队协作效率

28.社会变革潜力=对社会现状的批判性反思深度x大众意识觉醒程度。—批判理论反思

29.社会公平程度=资源分配均街度x机会均等程度x权利保障充分度。- 一社会公平程度

30.关系亲密度提升=给予对方价值x对方感知并回馈价值的可能性。——人际关系互惠原则

# 目标管理

人生、工作结果 = 思维方式 × 热情 × 能力

为何有一直盯着目标完成，而不是质量的强迫行为？

本质过程太单调，无法忍受过程，希望脱离痛苦
急功近利的思维模式好奇和热爱不够，创意不多
根据实际情况调整做事速度，尽量分解和重构，让任务简单易做

想对一个东西不厌倦
不断创造新的执行方法，不断思考怎么做有意思

减少压力增加执行力，不要盯着目标，要专注过程，当你一直看着目标去量化就会越来越急功求成，压力越大

要得到你想要的某样东西，最好的办法是让你自己配得上它。
自由从何而来？

人生最重要是休息、思考、精简

关闭屏幕，外出会让自己主动思考

大部分人来说，读书不过是消遣或者逃避要做的要事情，一种自我努力和感动的行为

真正的读书是要实践和验证书本的信息真伪，减少读书，验证知识更必要，禁止神话书本和知识，看书前先写一次书，防止先入为主，看书相当看答案

最好的学习方式，就是看完什么东西之后随手写点。 用自己的语言总结，越写越利索，而且对思维的训练也非常厉害，让知识条理化，让大脑清晰化。 尤其是跟时间叠加起来，你做一件事，做一周没啥用，两个月也没啥用，但如果能折腾五年，绝对能折腾出点啥来。

人类社会绝密三件事情
- 如何赚钱
- 怎么做人
- 发现真相


## 自信+幸福

几乎每个人的幸福感都是通过比较得来的
如果周围人过着跟我们同样穷的生活，即使住在透风漏雨的小屋子，我们也会觉得知足、幸福； 但如果周围哪怕有一个人过得比我们稍微好一点，即使又有一个融乐的家庭，舒适的工作，我们也会有强烈的不幸感。 ——《身份的焦虑》

从自信来，而自信则是从自律来！ 先学会克制自己，用严格的日程表控制生活，才能在这种自律中不断磨练出自信！ 自信是对事情的控制能力，如果你连最基本的时间都做控制不了，还谈什么自信？

自律的前期是兴奋的，中期是痛苦的，后期是享受的人的欲望是无穷无尽的，你必须很努力地克制，才能不至于失控。 和欲望的增长相比，能力的增长是极其的缓慢而艰难的，随便一个技能点，都需要一两年的时间去磨练，而一个欲望。可以在1天之内从地上飞到天上，即使是你一天之内增长的欲望，你可能用一辈子都无法用能力匹配上。

一切的成功，都是源于一个梦想和毫无根据的自信


自律是结果
自律是个很反人性的东西，光自律基本上做不成任何事

能够成事的重要因素，是激励、夸赞、成就感这类能让人感受到积极情绪的正向反馈。 自律只是正向激励的副产品。

人们有一种不健康的心理，喜欢把简单的事情复杂化

难点不在于没有新思维，而在于无法摆脱陈旧思想的束缚

要取得非凡的成果，没有必要非得做非凡的事情

独立思考的人一般有两个特点，一个就是杠精，一个是不合群。

世界不是由贪婪来驱使的，还是由嫉妒来驱动的

自制力从来不是要几点睡觉几点起床，从来是永远只做最重要的事。

在学习伟大前辈时真正重要的是：学习标准和原则，而不是去模仿表面的东西

如果你现在不觉得一年前的自己是个蠢货,那说明你这一年没学到什么东西。其实生活真的不需要比别人好，但一定要比以前好，比昨天好

真正的竞争优势更多的取决于你创新的速度，而绝非你可以遏制对方的发展程度！

想要成长，完美主义、失败洁癖和高姿态要不得

幸福感往往来自于期待

人们的幸福感，其实并不来自拥有财富的绝对量，而来自财富带来的社会相对“序位“、安全感与选择权这三个东西。

幸福感的一个标准，是每天早起时，都对新的一天要做的事情，有一种不假思索的期待

要接受自己的优点，也能接纳自己的不足

一个人情绪稳定的背后，是实力，也是格局。简单事不争吵，复杂事不烦恼。发火时不讲话，生气时不决策

一个人有多大的高度取决于对这个社会的认知程度
独立思考的人一般有两个特点，一个就是杠精，一个是不合群。

别人赞成也罢，反对也罢，都不应该成为你做对事或做错事的理由

1：3等NB规则内最优，2等NB改变规则，1等NB突破规则，绝顶NB创造规则，低能无视规则。NB的人是不会废话的，只会默默的砸烂你的人生观，价值观，社会观。

2：懂得低头的人，有可能将来混出尊严，穷的只剩下尊严的人，早晚连这份尊严都没了，清高和弱往往相生相伴，毁人不倦

4：“每个人给你一分钱会怎么样？” LOSER想的是他会富，NB的人想的是能否实现，怎么实现。十年前你觉得不可能，十年后马云让它成了可能。再过十年，还有更多洗刷你三观的事发生，但是没人会在十年前教你。

19：你和朋友被老虎追，怎么逃生？答案是给朋友一刀子，老虎就不会吃你了。这个道理可以解释非常多数社会现象。

> 真正的强硬
真正的强硬，不是说很凶和别人去吵架，或者赤膊上阵去干仗。那是一种表象上的强硬的幻觉。 真正的强硬，是能够冷静的保持和扩大自己的优势地位，对于各种可能瓦解自身优势地位的短期诱惑和陷阱，强硬的不为所动。 而别的不甘寂寞的意志薄弱者纷纷踩坑后，自然变得软弱，不攻自破。

> 管理的护城河

在任何团队，只要能带着打胜仗，那主帅的威信就高。实力和战果是一个主帅威信的最大护城河。什么关心下属、为人和善，这些都是次要的。一个团队最好的成长方式，就是不断打胜仗，且胜仗的规模越打越大。

不要走现成的道路，去那些没有路的地方，自己开辟出一条小路吧。



## 三件大事

财富=认知 x（权力+资本+劳动）

这个世界没有神话，只有一些很朴素的道理：便宜的打败贵的、质量好的打败质量差的、认真的打败轻率的、耐心的打败浮躁的、勤奋的打败懒惰的、有信誉的打败没信誉的

第一件：研究赚钱。
钱能解决99%以上的问题。走出舒适区的目的不是找罪受，而是找到一个你之前从来不知道的更舒适区域。

第二件： 研究人性。
只要看够人性，你就能在这个社会上立于不败之地。

第三件：爱护身体
叔本华曾说，一个人最大的错误就是，拿健康换取身外之物。

自由
自由从何而来？从自信来，而自信则是从自律来！ 先学会克制自己，用严格的日程表控制生活，才能在这种自律中不断磨练出自信！ 自信是对事情的控制能力，如果你连最基本的时间都做控制不了，还谈什么自信？要得到你想要的某样东西，最好的办法是让你自己配得上它。

你的人生由你打造，做独一无二的自己

> 运气

努力、勤奋、坚持这些品质，都会增加你接触运气的机会，所有的成功都是运气。而提升运气的有效方式，就是多试，量变产生质变。

所以：

- 多交一些朋友，多交流
- 多尝试一些方向
- 多提升自己的能力，改变执行的方法

> 时光机

请你闭上眼睛……想象一下，今天是2057年，您已经七、八十岁了，坐在空无一人的房间，臃肿老态，没钱没权，生活不能自理。您对佛祖许了一个愿望：“求求您，让我再年轻一次吧！”佛祖说：“好的。”

于是“嗖”的一声，您睁开眼睛一看，回到了2017年的今天?

也许现在的生活不是你想要的，但却是你自找的

人生有的时候并不那么复杂，尽量把时间和精力用在自己喜欢的事情上，最怕为了钱、地位、面子等，不愿意放弃其实并不喜欢做的事，这些永远不是人生的意义和目的，它们可以为实现你喜欢的事情服务。

## 像农民一样思考

1. 不要对着庄稼大喊大叫。--情绪稳定

2. 不要责怪庄稼长得不够快。--尊重规律

3. 不要在庄稼长成之前，就将其连根拔起。--耐心与等待

4. 选择最适合土壤的植物。--找对赛道

5. 灌溉并施肥。--用心与勤奋

6. 定期清除杂草。--第一性原理，抓大放小

7. 适时休憩

想要成长，完美主义、失败洁癖和高姿态要不得

## 反思和质疑


问下自己今天是否过得有意义有价值有学习有收获

一直以来我有个习惯，基本每天晚上都会问下自己今天是否过得有意义有价值有学习有收获，如果答案是没有，明天或者趁这天还没结束马上补救。这是个好习惯，也是个适合我的习惯。周五至今，虽然大量阅片和看书，但工作效率和思考明显不够，很是失望，刚才反思了一下，马上设计了工作的排序和轻重缓急，且立刻动起来，让这个周末反转成有意义。我这个笨猪。——经纬张颖

重要的是，一定不要停止质疑，好奇心自然有其存在的理由，我并不比别人更聪明，我只是比别人在问题上花的时间更多一点

千万不要相信，压力能转化为动力，压力只会转化为病历。人生真正的动力源自于，内心深处对快乐和兴趣的追求

> 屏蔽力
最顶级的能力是“屏蔽力”，任何消耗你的人和事，多看一眼都是你的不对。你的时间很有限，所以不要为别人而活。不要被教条所限，不要活在别人的观念里。不要让别人的意见左右自己内心的声音。一个珍贵的品质——不要在生活中对别人的生活指指点点

真正的安全感来源于你能为他人创造的价值；只有这样，你才能确保自己永远有饭吃

## 错误与恐惧

如果你脑袋里的想法都是对的，那为什么你口袋里没有你想要的?贫穷和失败往往不是导致人生悲惨的核心原因，导致人生悲惨的核心原因，就是你一直坚持着一个错误的想法，并且坚信它是对的。疯狂就是重复做相同的事情，却期待不同的结果

多数人停滞不前的主要原因之一，就是不断重复错误的行为，却总希望能得到正确的结果。

只要做事，就会犯错。做事越多，犯错越多。要想不犯错，除非不做事，找到你做得最好的那件事，反复做！

接受犯错，复盘、优化、改正。然后做好继续犯错的准备。 不要用发展中的错误去否定发展。不要用战术上的错误去否定战略。不要用执行人的错误去否定战术。不要用一时的失败去否定长远。

> 恐惧
解决恐惧的最好方法，就是时常面对这种恐惧，因为很多恐惧都是源于未知，你见得多了之后就不觉得稀奇了，毕竟你也不会恐惧一支牙刷、你们家的猫、你的枕头，天天见的东西多数是不会有恐惧的。 做其他很多事也一样，写作、拍视频、剪辑、

免费只是表面的免费，背后的潜在风险才是费用

人和人之间最大的差别就是对于工具的使用。你用枪，我用刀，我就打不过你。

当你全心全意为他人解决问题时，你成长的速度会比那些只关注自己利益的人快好几倍。

当你做成一件事时，说明你的方法用对了，没必要沾沾自喜；当时没有做成一件事时，说明你的方法有问题，也没必要妄自菲薄。对普通人来说，研究别人的失败、为什么会失败，进而令自身避免同样的失败，比研究别人的成功，价值更大。

想要事情做到十全必然会在做人方面减分。所以做事到8分即可。

又想起了两年前一位行业长者的话——少说话，多做事，坚持做短期不紧急但长期重要的事。

真正的竞争优势更多的取决于你创新的速度，而绝非你可以遏制对方的发展程度！

# 学习

所谓常识，往往不过是时代的偏见。要超越这个时代的偏见，唯一的办法，就是阅读，阅读人类历史上最伟大的经典著作。没读过几百本经典，不足以谈独立思考。

阅读
当有人问“我读过很多书，但后来大部分都被我忘记了，那阅读的意义是什么？”时 我的回答是：当我还是个孩子时我吃了很多的食物，大部分已经一去不复返而且被我忘掉了，但可以肯定的是，它们中的一部分已经长成我的骨头和肉。 阅读对你的思想的改变也是如此，坚持。

做事不要总是要求啥回报。学习也是一样，不要说我学习一样东西，就是一定要能够看到直接产生啥回报。 我就是兴趣广泛，就是喜欢做它，就是喜欢学习它，我就是不求什么结果，我就是不求什么回报，我就是就是为了好玩，我就是享受玩的过程，我就是就是图个开心。


“知道”和“懂得”是两码事，任何观点都有其成立的前提条件， 如果前提条件变了，观点就不合时宜。

不要轻信各种理论。他认为，第一，理论都是建立在假设基础上的，本来就不是事实；第二，理论到处都是，简直多如牛毛，让人无所适从

一般人学习理解是总是觉得做什么是最重要的，其实最重要的是不做什么

学做什么——这是追求速度的更快。 不做什么——这是避免负速度。

不要从别人的经验中学习，不管是古人还是今人
我为痛苦生活开出的第二味药是，尽可能从你们自身的验获得知识，尽别从其他人成功或失败的验中广泛地吸取教训，不管他们是古人还是今人。这味药肯定能保证你们过上痛苦的生活，取得二流的成就。

避免广泛吸取知识的另一种做法是，别去钻研那些前辈的最好成果。这味药的功效在于让你们得到尽可能少的教育。

我们的大脑就像一个装满蜜蜂的沙滩球，数百种不同的力量让我们前往不同的方向。 人们绝不会专心于一件事，我们总是想去完成所有事情 。我们想去锻炼的同时又想去学西班牙语，又想出去吃披萨。欲望是无穷无尽的，这些不受约束欲望，总在把这个球推向他们想要的方向。但是通常来说，那个球哪也去不了。它里面的着这些欲望没起到什么作用，

问题：
读书越多会越孤独吗？

答：
我忍不住要开地图炮了，知乎上有几种奇怪的价值观，其中之一就是将读书神圣化。事实上，大部分人的读书就和听音乐打游戏看电影追剧一样，是一种消遣和娱乐工具。

可笑的是，很多人打游戏看电视会产生虚度光阴之感，读起书来居然就觉得自己过得很充实。

大部分人的读书不过是兴之所至，纯粹追求精神上的愉悦和满足，读的过程还不怎么动脑子，读完也不反思作笔记。这样的读书，和打游戏有什么区别？

读书是要成体系的，你是谁，你想做什么，决定你该读什么书，怎么读书。题主的问题就是没有体系地读书，兴之所至，漫无目的，还觉得自己与众不同，别人都low爆了。

依我的观察，悟性高的人，哪怕从不读书，也可以从生活中领悟真知灼见；而本来悟性就低还没有方法的读书人，读得越多只会让他与周围的环境越发格格不入。

但可惜大部分人读了一些书就开始“孤芳自赏”了，觉得周围都是一群low逼，他们没发现生活中有一些优秀的人很少读书，但他们的一言一行比书中的道理更深刻。

人的本性就是不愿承认自己做的事是没有价值的。很多人将读书神圣化，批评别人功利，谈精神的追求，性灵的满足。知乎上对于读书更有很多清新脱俗的言论，而且已经统一了价值导向，譬如读书早晚会忘，为什么还要读书？ - 阅读这个里面的回答。

对此我只想说：既然你会忘，你为什么不思考不作笔记不写总结然后温习？

归根结底，因为对于大部分人来说，读书和打游戏是一样的。作为一种消磨时间的工具，他们当然不会这么郑重地对待它，而且还会有更多清醒脱俗的理由来辩解。

对这些所谓读书人而言，读书和打游戏唯一的区别是，打游戏会有虚度光阴的罪恶感，读书却有一种自欺欺人的充实感。

教育的真谛就是当你忘记一切所学到的东西之后，所剩下的就是教育。

## 学习的缺陷

不要以为你看到的就是真相，真相永远比你看到的要复杂，一定要扩大信息源，接触互相冲突对立的信息，接受专业的思维训练，才能明真相，辨是非。避免自己困在信息茧房里。否则人的思想、行为就会越来越固化、单一。

> 知识整理
我们每天都接受了大量的信息，大量的“知识”。如果“知识”没有消化，那么它们就不是知识。

不过笔记虽然多，我也只是当做它是高质量的数据库，有需要的时候再从中查询我需要的信息。现在觉得，也是时候需要整理一下，将最有价值的东西提取出来，化为自己的知识。

读书千万不要破万卷，破万卷容易造成读不深，读不透。读书更应该是破万遍。

如果历史书是致富的关键，那么福布斯400的富豪榜单将由图书管理员组成

在学习伟大前辈时真正重要的是：学习标准和原则，而不是去模仿表面的东西

所谓常识，往往不过是时代的偏见。要超越这个时代的偏见，唯一的办法，就是阅读，阅读人类历史上最伟大的经典著作。没读过几百本经典，不足以谈独立思考。

> 尽信书，则不如无书
《尽信书，不如无书》出自《孟子》的《尽心章句下》。 “尽信书，则不如无书。” 这是精辟透脱的读书法，要求读者善于独立思考问题。

做事不要总是要求啥回报。学习也是一样，不要说我学习一样东西，就是一定要能够看到直接产生啥回报。我就是兴趣广泛，就是喜欢做它，就是喜欢学习它，我就是不求什么结果，我就是不求什么回报，我就是就是为了好玩，我就是享受玩的过程，我就是就是图个开心。

如果一个道理是对的，写进书里，让所有人不需要门槛都能看到，那它就不那么靠谱了。所有理论都有门槛

批判知识和思维
### **一、培养批判性思维**

1. **质疑一切**  
不要轻易接受任何未经验证的信息。即使是权威来源或广泛传播的内容，也要保持怀疑态度，追问其背后的逻辑和证据。

2. **识别偏见**  
每个信息源都可能存在主观偏见或立场倾向。学会识别这些偏见，避免被单一视角误导。例如，媒体可能因商业利益或政治立场而选择性报道。

3. **分析论证结构**  
评估信息的逻辑是否严密：是否有清晰的前提、推理过程是否合理、结论是否符合事实。如果发现逻辑漏洞或情绪化表达过多，需提高警惕。

* * *

### **二、多渠道交叉验证**

1. **多元化信息来源**  
避免依赖单一渠道获取信息。尝试从不同立场、背景的媒体、学术研究、专家意见中获取信息，以形成全面的认知。

2. **对比与核实**  
对比多个来源的报道，寻找一致性和差异点。如果某条信息只出现在一个来源中，且没有其他独立信源佐证，则需要进一步核实。

3. **关注原始资料**  
尽量追溯信息的源头，比如查看原始研究论文、官方声明或第一手数据，而不是仅仅依赖二手解读或评论。

# 动机和利益方

1. **区分事实与观点**  
很多信息混杂了事实陈述和个人观点。学会将两者分开，优先关注有证据支持的事实部分。

2. **耐心等待完整信息**  
在突发事件中，初期信息往往不完整甚至错误。不要急于下结论，而是等待更多证据浮现后再做判断

受益方，夸大和好处归于嫌疑人

# 历史对比

过往类似历史事件

## 教学办法
最好的教学方法是什么？讲故事

讲故事好的地方在于，可以把未知的和已知的做类比，这样就更容易理解。毕竟所有的知识都来源于现实生活，世界上没有绝对的抽象，一切都能在现实中找到映射。

（只要是）我不能创造的，我就（还）没有理解。要知道怎么解决每一个已经被解决的问题。

尽量不做别人已经做好的事，不能比别人做得更好就不做，除非是业务防御关键点

人是唯一能接受暗示的动物。所以和勤奋的人在一起，你不会懒惰；和积极的人在一起，你不会消沉

把放在别人身上的希望收回来搁在自己身上，可保自己一世的浪漫。

为什么有的人年纪轻轻却思想深度远高于常人？
什么叫真正的思想深度？ 不是你会讲几句似懂非懂、看似很有内涵的“金句”；不是动不动就“看透人生”；更不是年纪轻轻就学会了世故和圆滑。 而是，你思考的东西是每一个人都困惑的东西，你要思考的目的，是如何为所有人的痛苦找到一条出路，更重要的是，你通过自己的思考，找到了这条路，并且一无所惧地带头走上了这条路。

## 知识的局限性

念念不忘必有回应，曾经询问ai多少知识才能够用，现在答案来了，能内化和高频使用的知识可能就1-3点那么简单，写作是一个情绪表达，按照情绪的方式制造阻碍让角色成功，引发观众共鸣即可

譬如拯救猫咪，核心就是一个节拍器，但其实即使是烂文章烂音乐都有自己的自拍，节拍有一定的旋律是一个结果，而不是参照这样的结果去套，要不就是公式，要不就是抄袭，根本不是正常的创作流程。

下面看一下知识实际的需要

知乎没有多少编剧。。。我胡说八道好像没有人管吧？编剧就是把多条故事线按照一定的顺序排列，与观众的情绪线相互融合。每个场景，每个镜头。在介绍背景的同时，既有故事又有情绪，形成一条完整的逻辑链。救猫咪￼这本书，把简单的道理复杂化了。这本书既不适合纯小白，也不适合高手。￼因为大多数小白编剧，都学不会那种埋了一堆伏笔，随着主人公的脚步，一步一步引爆故事核￼的手法。能用镜头把事情讲清楚，就已经很了不起了。对于高手而言，讲故事的手法没问题，但是最缺少的是故事核的爆点。这个爆点，就是通过单位时长的情感渲染￼，让观众流泪的瞬间。￼所以这本书就很尴尬。小白去学习，他不知道该如何排列组合故事线索，高手去学习，又想不出来场景下如何符合人设来一炮。￼所以救猫咪，特别像知乎的网文回答。比如开局要以主角为中心，尽快出现主角名字，迅速介绍主要矛盾点，不要大段出设定。。。（以下省略五万字）

最核心的一句是。。。以上五万字，大神可以不遵守。

可以看，不过，不建议迷信，因为这类经验类的东西，比较个人化，比如说此书也是作者结合自身的从业经验，创作经验而写出来的。然而，每一个人的创作习惯或许都不太一样。因此，这种形而下的部分不建议过分迷信复制。

相反，形而上的人类探索规律或自然规律就更可靠一些。所以建议题主先不要完全扎进这种经验类的工具书，先去体统了解了编剧学的相关的知识，之后再回头看看这些前人的经验谈。或许收货更大，理解更深刻。

在这里不得不多说一嘴，我发现不少职业人对”编剧学”有一个误会，认为编剧学就是停留在表层的比较简单的理论经验总结，认为把控把控结构，调整调整情绪节拍，那就是编剧学了。

emmm……实则不然，编剧学关注的不仅仅是临时解决问题的具体手段（虽然也有不少板块涉及此内容），而是一种根本上的思维方式，甚至牵扯到形而上的规律，尤其是在探讨生活与创作之关系的时候。

举一个形象的比喻的话：编剧学并不是教你如何复制《老人与海》，而是教你如何像海明威那样去思考。

所以，编剧学的研究挑战着很多固有观念，比如说，创作一定是感性的吗？我们如何拜托那种日夜煎熬守株待兔式地追寻灵感来产出的蛮荒时代？等等……

当然，这些理论其自身依旧有不少局限性，有待充实改进。但首要的问题，就是应该如何付诸实践并传递。

我不禁想到了前不久，刚和某某主编的聊天中，提到了毛泽东的《实践论》，事下仔细回味了一下，着实受益匪浅。

的确如此，”感觉到了的东西，我们不能立刻理解它，只有理解了的东西才更深刻地感觉它。感觉只解决现象问题，理论才能解决本质问题。这些问题的解决，一点也离不开实践。”——《实践论》毛泽东

在培养新人的阶段，企图跳过漫长的感性认知阶段，直接急功近利地将理性认知抛出来，实在过于急躁了。

作者的成长只有一条路，自己救自己。

行业里的圣经是[麦基]的《故事》

就像《故事》里说的，编剧的奥秘在于原理，而不是那些框架、规矩。

所以学习编剧，其实就像《魔教教主》里的太极，一定要把那套东西完全忘记，只记得感觉。

而《救猫咪》，啥都写了，偏偏没写感觉。

# 专注+专业+

很少见到有人专心致志地去完成一件美好而正当的事。我们通常见到的，不是畏首畏尾的学究，就是急于求成的莽汉。

最快的成功方法就是，保持对一件事情的专注，不停地重复，不停地深钻，只要看透时间的复利效应，每一个人都可以是一个小领域的佼佼者，怕的就是你在各个领域之间徘徊游荡

注意力>时间>金钱

最快的成功方法就是；保持对一件事情的专注，不停地重复，不停地深钻，只要看透时间的复利效应，每一个人都可以是一个小领域的佼佼者，怕的就是你在各个领域之间徘徊游荡

不算是什么行业，好像成功的底层秘诀就是热爱与专注。

不要用你的业余，去挑战别人的专业

最快的成功方法就是，保持对一件事情的专注，不停地重复，不停地深钻，只要看透时间的复利效应，每一个人都可以是一个小领域的佼佼者，怕的就是你在各个领域之间徘徊游荡

一个人情绪稳定的背后，是实力，也是格局。简单事不争吵，复杂事不烦恼。发火时不讲话，生气时不决策

自负和自我怀疑之间达到一种平衡，保持一定的自我怀疑，持续修正自己的观点，也能够直面自己的错误，不偏执

能够成事的重要因素，是激励、夸赞、成就感这类能让人感受到积极情绪的正向反馈。


## 专业能力

> 重要的事和可知的事
有些事情很重要，但不可知。宏观问题都属于这个范畴。有些事情是可知的，但不重要。我们不想让这些干扰我们的大脑。

所以，我们思考：什么是重要的？什么是可知的？有很多重要的事情，都没有答案，所以我们不去想它们。如果我们去关注这些问题，就会错过很多重要而可知的事情。

重要的是，一定不要停止质疑，好奇心自然有其存在的理由

不要让重要的事情变成紧急的事情。

2. 当然你的经历经验越多，开启其他的门也就更容易。

3. 仅理解世界还不够，还必须理解你自己，才能往前走。

5. 我们对现实的理解永远是不完美的、欠缺的、可改进的，而且永远会不完善。

6. 必须对自己的信念保持质疑的态度，与此同时又必须有某种信念。

8. 有些事是不能量化的，所以那些人在数字计算上做得很好，但忘记了现实。

9. 哲学本身不能帮你走很远，所以你必须要经历实践，这种哲学必须是自己形成的你对世界的看法。

唯有读书和赚钱，才是一个人最好的修行，前者使人不惑，后者使人不屈，所谓常识，往往不过是时代的偏见。要超越这个时代的偏见，唯一的办法，就是阅读，阅读人类历史上最伟大的经典著作。没读过几百本经典，不足以谈独立思考。

可能性，就是“未来的能力”
所谓“不可能”，只是现在的自己不可能，对将来的自己而言那是“可能”的。应该用这种“将来进行时”来思考。要相信我们具备还没有发挥出来的巨大力量。

所谓“不可能”，只是现在的自己不可能，对将来的自己而言那是“可能”的。应该用这种“将来进行时”来思考。要相信我们具备还没有发挥出来的巨大力量。

持续努力，天天钻研创新眼睛可以眺望高空，双脚却必须脚踏实地。梦想、愿望再大，现实是每天必须做好单纯、甚至枯燥的工作。在昨天的基础上前进一毫米、一厘米都要挥洒汗水，把横亘在前面的问题一个一个的解决。

无论做什么事，动脑筋改进的人与漫不经心的人相比，时间一长两者之间就会产生惊人的差距。在昨天努力的基础上再下功夫改进，今天比昨天稍微前进一步，想把事情越做越好，这种态度持之以恒，就能产生巨大的进步。不走老路，这就是逐渐取得非凡成就的秘诀。

像我们的工作也一样，要不断创新，不墨守成规，不走老路，不沉浸在原来成功的自以为最好的模式，才能走出原来的维度，才能升级到新的境界。

扎根在现场工作就能获得灵感工作现场有神灵，比如，为了攻坚克难，千方百计反复试验仍不得要领，不断碰壁，山穷水尽。然而当认为要绝望时，事情才刚开始，暂且让头脑冷一冷，再次回到现场，重新观察周围的情况。

答案永远在现场，但是要从现场获得答案，首先从心情上说，必须对工作有不亚于任何人的热情，有解决问题的深切期待。

同时必须亲临现场，用真诚的目光仔细的观察现场，用眼睛去凝视，用耳朵去倾听，用心灵去贴近，这时我们才能听到产品发出的声音，找到解决的办法。

凡事如果你能想着10年20年，你大概就不会那么困惑了

## 专业和业余

人的一生，其实就是时间和注意力分配的过程，你所专注的一切，反过来定义了你是谁.因此要格外珍惜你的注意力，认真选择你的情绪和思想所聚焦的对象。 如果你只是不停被喧嚣带走注意力，或总是纠结于错误的事与人，迟早你会被外界塑造或污染，成为你不想成为的人，白白浪费掉你仅有一次的生命。

为什么有一部分人可以成就非凡，而我们绝大多数人努力挣扎仍停滞不前？答案是复杂的，可能也是多面性的。

> 专业和业余
很重要的一方面是思维方式——特别是业余思维与专业思维的区别。

业余：目标导向。
专业：过程导向。

业余：自认为擅长一切。
专业：清楚自己的能力范围。

业余：将反馈和教导视为批评。
专业：清楚自己的弱点并且能够对症下药。

业余：孤立式思维——只考虑一次高难度的接球。
专业：连贯性思维——如果有 10 次高难度的球我能接住9个吗？

业余：不断关注自己的劣势并试图将其提升。
专业：聚焦于自身长处并结交能弥补自身短板的伙伴，才能达到最佳

业余：关注点在于贬低别人。
专业：关注点在于使别人变得更好。

业余：跟着脑海第一个闪过的念头走。
专业：知道第一个念头通常不是最好的。

业余与专业的区别很多，不过可将其归结为两点：畏惧和现实。
专业者意识到人需要适应世界、与外界合作，似乎可以对所有事情都处理恰当。

## 专注

最快的成功方法就是；保持对一件事情的专注，不停地重复，不停地深钻，只要看透时间的复利效应，每一个人都可以是一个小领域的佼佼者，怕的就是你在各个领域之间徘徊游荡

当你做成一件事时，说明你的方法用对了，没必要沾沾自喜；当时没有做成一件事时，说明你的方法有问题，也没必要妄自菲薄。

很多看似荒诞的东西，其实是有逻辑的。只是，逻辑被隐藏了起来，或者，你看不到。

真实的捷径
很多希望我介绍绍“术”的人是想走捷径。但是真正做好一件事没有捷径，离不开一万小时的专业训练和努力。做好搜索，最基本的要求是每天分析1020个不好的搜索结果，累积一段时间才会有感觉。我在 Google改进搜索质量的时候每天分析的搜索数量远不止这个，Google的搜索质量第一技术负责人阿米特・辛格(Amit Singha1)至今依然经常分析那些不好的搜索结果。但是，很多做搜索的工程师(美国的、中国的都有)都做不到这一点，他们总是指望靠一个算法、一个模型就能毕其功于一役而这是不现实的。

From: 《数学之美》吴军

> 欲望与分散
我们的大脑就像一个装满蜜蜂的沙滩球，数百种不同的力量让我们前往不同的方向。

人们绝不会专心于一件事，我们总是想去完成所有事情。我们想去锻炼的同时又想去学西班牙语，又想出去吃披萨。欲望是无穷无尽的，这些不受约束欲望，总在把这个球推向他们想要的方向。但是通常来说，那个球哪也去不了。它里面的着这些欲望没起到什么作用，而关键却在于地形。

这是大多数人度过人生的方式，这些欲望在无休止地冲突，我们永远没有足够的时间去实现。结果就是我们没有能力去战胜面对的困难。

有好的点子还不够，很多人有好的点子，问题在于太多这些想法在一起，就会相互抵消了。

> 自由从何而来？
从自信来，而自信则是从自律来！先学会克制自己，用严格的日程表控制生活，才能在这种自律中不断磨练出自信。自信是对事情的控制能力，如果你连最基本的时间都做控制不了，还谈什么自信？

专注和简单一直是我的秘诀之一。简单可能比复杂更难做到：你必须努力厘清思路，从而使其变得简单。最终这是值得的，因为一旦你做到了，便可以创造奇迹。

> 想象力
有人问刘慈欣怎样才能有丰富的想象力，他回答：首先你得找个既有钱又过得清闲的工作，你要是每个月都从银行拿利息，在家无所事事，想象力肯定会丰富起来，成天为生计奔忙的人想象力是不会丰富的。

# 产品+营销

竞争的本质在于产品差异化，要做别人提供不了的东西。没有差异化，就成了日用基础商品，只能靠价格来竞争，很难挣钱。 ——段永平

福建流传一个调侃莆田系医生接待患者的12字营销方针：
你有病（表情要惊愕、语音要夸张）、病很重（沉重、摇头）、还有救（拍胸脯、很自信）、药很贵（图穷匕见）

## 简单就是美

> 赚钱的逻辑

v＝n（r-c）f

v代表赚钱的速度
n是满足的需求的客户总数
r代表单次满足需求得到的回报
c代表单次满足需求得成本
f代表满足需求得频率

KISS 原则是用户体验的高层境界，简单地理解这句话，就是要把一个产品做得连白痴都会用，因而也被称为“懒人原则”。换句话说来，“简单就是美”。

较简单的系统更容易构造、运行和维护；

较简单的解决方法总是更具弹性、柔性；

较简单的系统更便宜；

较简单的系统更容易被更快地实现、获得更快的回报；

较简单的方法使用者更喜欢；

较简单的系统更容易分阶段地执行；

较简单的系统更容易被使用者了解。

## 莆田系12字营销

福建流传一个调侃莆田系医生接待患者的12字营销方针：

你有病（表情要惊愕、语音要夸张）、病很重（沉重、摇头）、还有救（拍胸脯、很自信）、药很贵（图穷匕见）。

投资顾问在面向银行高净值客户时，这一营销策略大概也是有效的吧。

# 人际关系

与人建立关系的正确顺序: 先展示实力，再展示原则，最后传递友好。 这三步一步都不能错，顺序错了就比较麻烦了。

世界上凡是人群聚集的地方，谈论的话题无外乎三个：拐弯抹角的炫耀自己，添油加醋的贬低别人，相互窥探的搬弄是非——《百年孤独》

人是唯一能接受暗示的动物。所以和勤奋的人在一起，你不会懒惰；和积极的人在一起，你不会消沉

人性最大的恶，恨你有、笑你无，嫌你穷、怕你富。国人与国人第一次见面，就打量对方的身份、身价，然后再选择，是给对方跪着、还是让对方给他跪着。

不要和别人比，要和自己比，这就是成长型思维

不要在熟人面前炫耀你的成绩。没人喜欢看到熟人发达了。绝大多数熟人觉得，你和他也差不多，凭什么你发达了？看你就不舒服。


## 求助

只有服气，才能争气，才能吐气。必须正视自己的底子不如别人，必须虚心向别人学习好的管理经验，好的技术

一去接近成功人士，让他们的想法影响你；二走出去学习，让精彩的世界影响你；世间没有贫穷的口袋，只有贫穷的脑袋！并且哈佛大学有一句名言：当你为自己想要的东西而忙碌的时候，就没有时间为不想要的东西而担忧了！

谋事，找手头宽裕的人；做事，找手头拮据的人

内心强大的人都有三个共性“不和他人作比较”“不对他人有期待”“不批判他人”。而内心脆弱的人则是相反。因为太在意他人，所以容易被他人所影响，心态像豆腐一样脆弱

做事要找靠谱的人，聪明的人只能聊聊天
与靠谱的人在一起，我们总是如沐春风，格外安心。如何判断一个人是否靠谱呢？通常有这9个特点：守信、守时、勇于认错、不在背后谈论八卦、心中有他人、不吹嘘、足够真诚、不占小便宜、有一定的能力。“做事要找靠谱的人，聪明的人只能聊聊天。”

人最大的运气，不是捡到钱，而是你遇到一个人，他打破了你的认知，带你走上更高的境界，这就是你的贵人。

凡事靠自己摸索，进展是非常慢的。
就算你是天才，也很难比得上踩在另一个天才肩膀的普通人，薪火相传的力量太大了。

所以与人交往要有求道之心，尤其是面对高人，要像海绵一样把他吸干。

如果有可能，最好跟他工作一段时间，近距离观察这个人，成功的秘诀是什么。

你的巅峰，可能还达不到别人的日常，与其苦思冥想，不如近距离学习，效果来的更快更好。

## 保留神秘

说话只要声音一低，你的声音就会有磁性，说话只要一慢，你就会有气质，你敢停顿就能显示出权威，任何时候都不要紧张，永远展现出舒适放松的状态。您永远把任何您想接触的人当成老朋友交谈就行了

不说自己目标，不说自己钱财， 不说自己家事，不说自己错误。

距离产生美，模糊也产生“信仰”。
对所有人——夫妻、朋友、老板、同事，要保持30%的神秘
部分神秘感是魅力的源头。逐步释放神秘，同时提升自己。新提升的部分，也是神秘感的增量 。

屁股决定脑袋，一个人所处的位置，决定了他的想法

做正确的事实际上是通过不做不正确的事情来实现的



人性最大的恶，恨你有、笑你无，嫌你穷、怕你富。国人与国人第一次见面，就打量对方的身份、身价，然后再选择，是给对方跪着、还是让对方给他跪着。

人生有的时候并不那么复杂，尽量把时间和精力用在自己喜欢的事情上，最怕为了钱、地位、面子等，不愿意放弃其实并不喜欢做的事，这些永远不是人生的意义和目的，它们可以为实现你喜欢的事情服务。

对普通人来说，研究别人的失败、为什么会失败，进而令自身避免同样的失败，比研究别人的成功，价值更大。

普通人的大脑没有接受过专业训练，生来就是被玩弄的命运

一定要扩大信息源，接触互相冲突对立的信息，接受专业的思维训练，才能明真相，辨是非。避免自己困在信息茧房里。否则人的思想、行为就会越来越固化、单一。

辛苦不赚钱、赚辛苦钱、赚钱不辛苦

一辈子都要和别人比较是人生悲剧的源头
”不要拿自己的缺点和别人的优点比较；可以去羡慕别人，不要去嫉妒别人，我们遇到的每个人，要去发现别人的优点，然后去学习，看到别人的缺点，要联想到自己，在自身避免。

这个世界上只有少数人真正活过，剩余的人基本在消费商业创造出来的二手人生

在专业领域，大多数让你觉得很优雅、B格很高的人，都是华而不实的。他们就像是衣服崭新、油头粉面、皮鞋擦得发亮的士兵。 真正的大佬，往往看起来很普通、朴素，其貌不扬。 警惕那些有着漂亮羽毛的鸟，他们最擅长的就是包装自己、迷惑别人。

千万不要以为我们割掉了毛发，穿上了西装，满嘴的英语，洋人就会高看我们，恰恰相反，当一个中国人，西化成一个洋人的时候，恰恰会引起他们的蔑视。只有让他们看到，我们中国人有着他们与众不同的文明与精神，他们才会在心里对我们有真正的尊重。

## 论友情

世界上没有真理，只有视角，人一旦悟透了就会变沉默，不是没有了与人相处的能力，而是没了逢场作戏的兴趣

只要不关注任何人的动态，不揣测任何人的想法，不去设想一些没发生的事情，简单点，钝一点，慢一点，你会发现你过得很自在

鲁迅：村里的狗叫了，其它的狗也跟着叫，但它们不知道为什么叫。当浑浊成为一种常态，清白就是一种罪。

不管你多么善良，当你没有价值时，就算你温柔得像只猫，别人都嫌你掉毛，不要浪费时间模仿别人的个性，更没必要重复别人的每句话，那只是鹦鹉学舌

如果你发现，大家都对你客气了，不是因为大家素质提高了，而是因为你变强了。在这个世界上，从不缺丛林法则，缺的是公正。心软和不好意思，只会杀死自己。理性的薄情和无情才是生存利器。

嘴甜的人尽量不深交，经常诉苦的人一定是废物，自来熟的人一般不值得信任，强大的人不会说悲惨的经历，爱发朋友圈的人大多喜欢炫耀，满口仁义道德的人一般都是自私！

大部分有钱人的确挺努力的，但是努力在于动脑和沟通，不体现在身体劳动。

做人和做事很多时候是相抵触的
想要事情做到十全必然会在做人方面减分。所以做事到8分即可。

一见面就问你谋生的人，本质上是在算计对你的尊重程度

不要和别人比，要和自己比，这就是成长型思维

一个很反常识的现实：你越强，这个世界对你的善意就越多。 你越弱，这个世界越会把你生吞活剥。 永远不要让他人掌控你的命运。人一定要自强不息，自己强大是最重要的。

嘴甜的人尽量不深交，经常诉苦的人一定是废物，自来熟的人一般不值得信任，强大的人不会说悲惨的经历，爱发朋友圈的人大多喜欢炫耀，满口仁义道德的人一般都是自私！

如果你穿漂亮的衣服，他们认为你在炫耀；如果你穿简单的衣服，他们认为你很穷；如果你说出你的想法，你就是粗鲁的；如果你保持沉默，你就是懦夫；如果你成功了，你就是傲慢的；如果你在挣扎，那就是懒惰。 你看看，无论你在生活中做什么，他们总是有话要说，所以忽视他们的观点，因为只要你的上帝对你满意，别太在乎别人的看法。

当你全心全意为他人解决问题时，你成长的速度会比那些只关注自己利益的人快好几倍。

在工作中，一些人会因为你的地位、权力和天赋主动和你交往，但是如果你对他们来说不再有用，他们离开你的速度是接近你的速度的12倍。

相反，在工作中，一些人会因为你的地位、权力和天赋主动和你交往，但是如果你对他们来说不再有用，他们离开你的速度是接近你的速度的12倍。我在腾讯担任副总裁时，每天约我吃饭的各种人推都推不走，一些人甚至要把房子和车子借给我。但是，当我离开腾讯，对那家公司不再有影响力时，90%的人没有再和我打一次招呼。不仅我有这样的经历，稍微有一点儿资源和权利的人都或多或少遇到过。我的一些学长曾经担任中国一些资产上千亿的国有公司的负责人，在位时上门的朋友赶都赶不走，一旦退休没了权利，连一个一同打高尔夫球的人都找不到。对于这种现象，你也不必奇怪，因为首先考虑自己的利益是人的本能，只要不刻意伤害我们，就不必太在意。你将来事业有成，也会遇到这种情况，对比一下，你就会慢慢体会挚友多么难得。

From：吴军《态度》

人性最大的恶，恨你有、笑你无，嫌你穷、怕你富。国人与国人第一次见面，就打量对方的身份、身价，然后再选择，是给对方跪着、还是让对方给他跪着。

内心强大的人都有三个共性“不和他人作比较”“不对他人有期待”“不批判他人”。而内心脆弱的人则是相反。因为太在意他人，所以容易被他人所影响，心态像豆腐一样脆弱

你没必要和别人杠，由于知识的贫穷，他只活在自己的世界里，只认自己的那个想法，你很难改变他！

马斯克：从不申请专利，那是弱者的游戏，对推动科技发展毫无意义。真正的竞争优势，是你创新的速度，绝非遏制别人的发展。

如果显而易见这是一个错误，那就去快速地纠正它。在你等待的期间它并不会变好。

多谈谈你的失败历程、少吹嘘你的成功经历，这样对你好。

别愚弄你自己，而且要记住，你是最容易被自己愚弄的人。

我比大多数人更成功的部分原因是，我善于破坏自己最喜欢的想法。

一切的成功，都是源于一个梦想和毫无根据的自信。——孙正义

乐观地设想、悲观地计划、愉快地执行

这个世界根本不存在“不会做”、“不能做”，只有“不想做”和“不敢做”，当你失去所有依靠的时候，你自然就什么都会了没有行不行的区别，只有做与不做的区别。

大人物造势，小人物借势，普通人要随势。很多时候，他人对你的敬畏取决于你是否有低成本伤害他人的能力； 另一些时候，他人对你的尊重取决于你是否有创造共同利益的能力； 如果你两头都不沾，那就是无能，纵使说的再有道理也不过是放屁。

## 亲密关系

家庭的第一核心， 永远是经济而不是感情

家庭成员尽量不要一起工作。 两代成年人尽量不要一起生活

家境越差，嗓门越大；生活越艰苦，对家人越爱用反问句沟通。

别去操心他人，哪怕是你的亲人，多年形成的认知，并非你只言片语能唤醒的，人难以被叫醒，唯有痛了才会醒。 管他人一分，人家就会恨你一分，故而，莫管他人，多管好自身。 这个世界上最难的两件事情，

家里从商从政的，一定要听父母的；从工从农的，一定要远离父母的认知

要记住，每个人都有义务捍卫、遵守或完善原则。如果你认为这些原则无法正确解决问题和争议，你应该努力修正原则，而不是肆意妄为。

有效利用社交媒体，应当是个很好的手段。你总是会有自己特别感兴趣，喜欢钻研的事情。把相关的思考和经历分享出来，总是可以找到志同道合的朋友。而且开放网络之上，总是会有一些新东西，给你点滴的意想不到的惊喜。

不要花太多时间去处理关系，有了钱，有了本事，一切关系都会很顺畅，如果不顺畅，那是因为钱跟本事还不够

恨人有，笑人无；嫌人贫，怕人富

对于随时可能反悔，你又无法制约他的人来说，最好的办法是别和他做交易。要反复无常，不要虔诚地做你正在做的事。

改变自己的第一步，不是洗心革面，来一次彻头彻尾的大改造，而是把以前想改变但半途而废的事做起来。比如把买了没读的书先看完，报了名没认真学的课去学一遍

能够成事的重要因素，是激励、夸赞、成就感这类能让人感受到积极情绪的正向反馈。自律只是正向激励的副产品。

一个东西如果可以精确测量，那么就更容易优化
人的行为模式，很大程度上是潜意识在优化那些容易测量的指标。 比如说以前没有时钟，大家对时间的概念非常模糊。随着钟表的出现和普及，并越来越精确，“浪费时间”这个概念就越来越强烈。 钱是最容易测量的，所以鼓励大家赚钱也好理解。

要接受自己的优点，也能接纳自己的不足

用资源跟你换资源的人，是厚道的； 用金钱跟你换资源的人，是诚恳的； 用感情跟你换资源的人，是狡诈的。

考虑到人可以从错误中学习，那么，最好的事情就是从别人的错误中学习
难点不在于没有新思维，而在于无法摆脱陈旧思想的束缚

如果没有什么值得做，就什么也别做

> 一辈子都要和别人比较是人生悲剧的源头
”不要拿自己的缺点和别人的优点比较；可以去羡慕别人，不要去嫉妒别人，我们遇到的每个人，要去发现别人的优点，然后去学习，看到别人的缺点，要联想到自己，在自身避免。

摩拜CEO：普通人+普通人+无限多普通人=0
只要优秀的。三个臭皮匠永远顶不了一个诸葛亮。优秀的人和普通人就是1和0的区别，再多普通的人加在一起，结果都是0。

心软和不好意思，只会杀死自己。理性的薄情和无情才是生存利器。

开始让人舒服的，一定是言语；后来让人舒服的，一定是人品。生活不全是利益，更多的是相互成就，彼此温暖。人与人的关系一定是，敬于才华，合于性格，久于善良，忠于人品。

如果你发现，大家都对你客气了，不是因为大家素质提高了，而是因为你变强了。在这个世界上，从不缺丛林法则，缺的是公正。

信息差，执行差，资源差，能力差，认知差

总是有很多条道路适合你，所以不要固守一条道路

只要利益不产生冲突，别人讲的话，一般不需要反驳
一见面就问你谋生的人，本质上是在算计对你的尊重程度

真正的竞争优势更多的取决于你创新的速度，而绝非你可以遏制对方的发展程度！

养成在清醒状态下先攻克难题的习惯，我的意思是，无论何时——早上、下午，或是晚上，只要发觉自己的状态适宜做任何事，就先做最困难的事，那么，你就会慢慢掌握集中注意力的方法

最糟糕的人生，就是晚上思考千条路，早上醒来走老路。

最顶级的能力是“屏蔽力”，任何消耗你的人和事，多看一眼都是你的不对。

一般人学习理解是总是觉得做什么是最重要的，其实最重要的是不做什么


## 见识

> 一切人生见识，都是数量问题。

圈子，决定了生活质量
多和漂亮的人接触，久了你都不好意思邋遢。
多和有上进心的人接触，久了你都不好意思偷懒。
多和会赚钱的人接触，久了会发现有钱人还那么拼。

主要是没搞清楚人生的真相之一。 大部分人不存在智商碾压，就四个字——熟能生巧 你到高档的地方去多了，你就熟悉了那套规则，你不紧张了，大家就说你见过世面了，说你优雅！说你体面！说你高贵！ 你恋爱谈多了，见到妹子不紧张

你到高档的地方去多了，你就熟悉了那套规则，你不紧张了，大家就说你见过世面了，说你优雅！说你体面！说你高贵！

你恋爱谈多了，什么牛马蛇神都见过了，男人就觉得你不怯场不好拿捏，一个人只要有机会试错，他就没可能一直是个low逼。穷人的关键卡壳就卡在这里，没有试错的资格，于是，就各种技能看起来都不行。

真正能给你撑腰的，是丰富的知识储备，足够的经济基础，持续的情绪稳定，可控的生活节奏，和那个打不败的自己。

# 执行力

一天可以分成两个部分：
1. 思考什么是正确的事。
2. 重复做正确的事。

几乎任何事情都是越做越简单，越想越困难，越拖着越想放弃，人们低估了那些简单大道理的重要性。凡事往简单处想，往认真处行。

72小时法则：筛选待办事项时，仅保留未来72小时内可启动的任务，避免清单臃肿117。

多数人停滞不前的主要原因之一，就是不断重复错误的行为，却总希望能得到正确的结果。

做事不设条条框框，没有太多自我要维护，经常能打破常规，非常努力，不妥协，不圆滑世故

去做一些简单的事情，而不是去解决难题，要去除时间因素，不知不觉解决问题，在心流中超越时间

与其急匆匆赶路，不如适时停下来做好规划
人们常说时间就是生命，但很多人却把时间花在“横冲直撞”上，一味向前跑，却省掉了“思考”的步骤，这对自己来说其实是一种无形的消耗。与其急匆匆赶路，不如适时停下来做好规划，反而更能提高效率

慢慢改，先开始

愚蠢就是重复做相同的事情，却期待不同的结果

成事的关键是“不着急、不害怕、不要脸”

一个粗糙的开始，就是最好的开始
不要等准备完美了再去做，因为完美的准备永远做不到。 先去做，再慢慢的改，无论哪个领域，都是先开始，再慢慢改进。 只有行动起来，才会发现自己的潜力。随着时间的推移，在突飞猛进。

世界上最不缺的两种人：点子家，精神画家，你可以继续耽误一秒钟，因为下一秒你还会耽误掉。只不过NB人的1秒钟永远比你珍贵。

不做清单
所谓要做对的事情实际上是通过不做不对的事情来实现的，这就是为什么要有stop doing list，意思就是“不做不对的事情”或者是立刻停止做那些不对的事情。也许每个人或公司都应该要积累自己的stop doing list。如果能尽量不做不对的事情，同时又努力地把事情做对，长时间（10年、20年）后的区别是巨大的。

两件阻碍我们自由的事：活在过去和观察他人

如果你发现了与自己信念相反的证据，就在30分钟内把它写下来，否则你的大脑就会把它屏蔽掉。我想说，人们对新证据有很强的抵抗力。

只有不断提升认知，持续与惰性对抗，才能真正掌控自己的命运难点不在于没有新思维，而在于无法摆脱陈旧思想的束缚，

每一天都比昨天更聪明一点

## 永远主动出击

永远主动出击，把你好的一面展现出来，把你的欲望展现出来。 即使没成功也无所谓，不过是几条消息、几个电话的事。 但是对方记住你了，一旦有其他机会，你会出现备选名单的最上方。

我们能成功，不是因为我们善于解决难题，而是因为我们善于远离难题。我们只是找简单的事做。

要允许自己犯错，人做事，就必然产生错误。因畏惧错误而不去做事，你的能力就会停滞不前，这比犯错误本身代价更大。

我不需要别人认可我，只要市场认可我就足够了

降低一个东西对你负面影响的最有效的方法永远只有一个，就是远离这个东西。

内心强大的人都有三个共性“不和他人作比较”“不对他人有期待”“不批判他人”。而内心脆弱的人则是相反。因为太在意他人，所以容易被他人所影响，心态像豆腐一样脆弱

人的欲望是无穷无尽的，你必须很努力地克制，才能不至于失控。 和欲望的增长相比，能力的增长是极其的缓慢而艰难的，随便一个技能点，都需要一两年的时间去磨练，而一个欲望。可以在1天之内从地上飞到天上，即使是你一天之内增长的欲望，你可能用一辈子都无法用能力匹配上。

如果你是个有追求的人，你的追求可以量化成一个具体的长期目标，可拆分为年目标、月目标、周目标、日目标。 理论上，你的每天都应该是忙碌而充实的。如果你感到无所事事或空虚无聊，那么一定是哪里出了问题。

你没必要和别人杠，由于知识的贫穷，他只活在自己的世界里，只认自己的那个想法，你很难改变他！

当你全心全意为他人解决问题时，你成长的速度会比那些只关注自己利益的人快好几倍。成功的秘诀就是认认真真做事，关注到每一个细节。

预测未来的最好方法，就是去创造未来

## 执行力

一旦发现那个对的方法，不断重复就是了，几乎任何事情都是越做越简单，越想越困难，越拖着越想放弃

你所浪费的今天，是昨天死去的人奢望的明天，你所厌恶的现在，是未来的你回不到的曾经。

变量复利
所有的成功都是运气。 努力、勤奋、坚持这些品质，都会增加你接触运气的机会。 而提升运气的有效方式，就是多试，量变产生质变。 所以： - 多交一些朋友 -

小事多和人商量，大事自己拿主意，如果觉得一件事值得你努力，就直接去做，尽量少跟周围的人商量

## 加速失败

马斯克有个说法叫“加速失败”，也就是快速迭代，不要怕出问题，出了问题赶紧改，用不了多久就能上一个新台阶。 航天工业在过去几十年里一直没啥进展，商业化太差是一方面，另一方面也是因为实在是输不起，尤其是NASA这种政府机构，失败几次领导就得下台了，所以领导们优先求稳。

让人放心，是一个人最核心的竞争力，也是一个人最出色的履历。有担当、能扛事，有原则、守规矩，讲信用、能力强……对自己负责，让别人放心，人生之路才能越走越宽广，越走越顺畅。

时间越长运气比重越小

这个世界根本不存在“不会做”、“不能做”，只有“不想做”和“不敢做”，当你失去所有依靠的时候，你自然就什么都会了

做好小事，熬过难事，静成大事 乐观地设想、悲观地计划、愉快地执行

## 高手练习

成长
就像下棋一样，假如你用心研究，复习，不断挑战自己，也许可以成为一名大师。但不断用懒散的方式去玩棋，下20年也只是一个“臭棋篓子”。

刻意练习有两个重要的概念，一是要始终专注；二是要不断给自己新的挑战。

高手努力时，是专心致志地做一件事，绝不一会儿刷微信，一会儿聊天，一会看视频；而普通人努力时，可能把手机就放在身旁，每隔半小时就看一下，本来只想看一眼，结果一看就是十分钟。

高手的努力是不断突破舒适圈，挑战自己的能力极限，让自己每天都有进步；而普通人的努力是待在让自己最舒服的地方，把自己已经会的东西重复一万遍。
这样下来，高手一直在进步，而普通人则一直原地踏步。

但实际的情况是，高手在专注于挑战自我的时候，很容易进入「物我两忘」的「心流」状态（即「最优体验状态」），并且看得见自己的进步，从而总是充满欣喜。 而普通人则根本不知道心流为何物，日复一日的努力，带来的只是无聊，以及看不到进展的焦虑。

> 我做过的最浪费时间的事

1. 开一个不需要发言的会；
2. 非要看完一本不值得的书；
3. 一直等人回我的微信；
4. 强行熬夜导致第二天精神不佳；
5. 期待别人和书给我答案，而不是自己去做调研；
6. 研究一个一劳永逸的策略

## 犯错

只要做事，就会犯错。做事越多，犯错越多。要想不犯错，除非不做事

有人之所以从来不犯错，那是因为他从来没有尝试过新事物

能改的叫缺点，不能改的是弱点！

接受犯错，复盘、优化、改正。然后做好继续犯错的准备。 不要用发展中的错误去否定发展。不要用战术上的错误去否定战略。不要用执行人的错误去否定战术。不要用一时的失败去否定长远。

未经思考的努力，才是我们贫穷的根源

慢慢改，先开始

要允许自己犯错，人做事，就必然产生错误。因畏惧错误而不去做事，你的能力就会停滞不前，这比犯错误本身代价更大。

宏观是我们要承受的，微观才是我们可以有所作为的
有些事情很重要，但不可知。宏观问题都属于这个范畴。有些事情是可知的，但不重要。我们不想让这些干扰我们的大脑。 所以，我们思考：什么是重要的？什么是可知的？ 有很多重要的事情，都没有答案，

老样子，不争论，不抱怨，不急不慌，不争不抢，做好自己的事，过好自己的日子

一张厕纸，用到的只有10%，剩下的90%是为了手不碰到屎； 交易里，有效的时间只有5%，95%的时间都是在等，就是为了不想碰到屎。

如果你读懂了冯柳，就知道“抄作业”没有用
我读过很多年的古籍和历史，里面有很多人生与故事，印象最深刻的就是环境和命运真的很重要。同样的事情，相同的选择可以有完全不同的结果、相反的却也可以有相似的结果，所以个人的判断是很微不足道的，要习惯把自己交给命运，要善于把自己托付给信仰和信念，有一个适应巨复杂体系的世界观，但也要找到简单而坚定的信仰或人生观、价值观做依托，这样才不容易陷入焦虑、迷惑与自责之中。

推迟满足、控制欲望最好的办法，就是跟它保持一定的距离
如果沉迷睡前玩手机，可以将手机放到其他的房间，改成看书、听音乐等。 如果长期暴饮暴食，可以减少购买零食，改成吃健康的水果蔬菜。

走出舒适区的目的不是找罪受，而是找到一个你之前从来不知道的更舒适区域。顺着天赋做事，逆着本能成长

间歇性努力，不是真正的努力。为何间歇性努力给人的感觉总是效果不长久，因为看起来你从来没有放弃过，事实却是，你一直在缓慢起步。当你有各种借口想要停下来的时候，告诉自己再坚持一下，都已经努力这么久了，别轻易放弃。



## 偏见

“创造者”=“理想家”+“务实思考者”+“坚毅者”
能提出独特和有价值的愿景，并以美妙的方式实现愿景（通常在他人的质疑下）。塑造者既能看到全局，也能看到细节，塑造者=理想家+务实思考者+坚毅者。

你的人生由你打造，做独一无二的自己



做好小事，熬过难事，静成大事

长期主义并不是不做好短期执行的借口

# 困难解决

我最近碰到一个同事，他前段时间其实也遇到了瓶颈。他前两天又找我，说的第一句话讲就说：「辉哥你前段时间给我说一句话，我特别认可，你说 —— 当你觉得困难的时候，你正在走上坡路。」

老实说这句话我忘了自己在什么时候给他讲的，忘记了具体的场景，但我知道他当时工作上遇到了瓶颈，而且甚至严重到有可能想换工作的程度。

我当时告诉他说：「你现在所遇到的所有瓶颈，仔细想一想，都是源于自己能力不足造成的，如果你能力够强，就不会遇到这个问题。所以当你能克服你的问题，能解决当下问题，你就向上走一步。而你放弃离开，你失去了这次成长的机会。」

恐惧
解决恐惧的最好方法，就是时常面对这种恐惧

所谓要做对的事情实际上是通过不做不对的事情来实现的，这就是为什么要有stop doing list，意思就是“不做不对的事情”或者是立刻停止做那些不对的事情。也许每个人或公司都应该要积累自己的stop doing list。如果能尽量不做不对的事情，同时又努力地把事情做对，长时间（10年、20年）后的区别是巨大的。

棘轮效应
棘轮效应（Ratcheting effect），是指人的消费习惯形成之后有不可逆性，即易于向上调整，而难于向下调整。

问对了问题就解决了一半难题
很少见到有人专心致志地去完成一件美好而正当的事。我们通常见到的，不是畏首畏尾的学究，就是急于求成的莽汉。

有人之所以从来不犯错，那是因为他从来没有尝试过新事物



## 自我批判

真心接受批评不是对他人让步 而是自己真正成长，自我批判，不是自卑，而是自信，只有强者才会自我批判。也只有自我批判才会成为强者。

1）周围确实没人有水平能批评自己，说明个人总与水平低于自己的人抱团，曰“关起门来做土皇帝”；

2）周围有人有水平能批评自己，但他们因利益有求于个人而不批评，曰“狗肉朋友”or worse；

3）周围有水平也无利益却不批评自己，那是心凉了。

提前准备
如果你想今天彻悟，那么昨天你就应该探索

## 困难解决

当你遇到一扇被锁着的门，你应该去哪找钥匙？显然不应该是只盯着锁头看，对吧？

所以，能开锁的钥匙一定在别的地方，否则就和没上锁毫无区别了。

1.坐享：定时十五分钟，以后慢慢加长；找一个舒服的姿势，期初不一定要用不习惯的“正确姿势”，但最好“挺直脊背”；闭眼；缓慢均匀的呼吸，不一定要刻意控制；把所有的注意力都放在呼吸上，别在乎自己走神，发现自己走神了就再回来。

2.万能钥匙：当你遇到被锁上的锁头的时候，要想到你应该去别的地方找钥匙……

3.当我们尝试解决问题的时候，只盯着问题想，盯着问题去找解决方案，通常只能以失败告终。当发现自己只盯着问题本身思考的时候，自己的元认知能力就会被激活。得把注意力从问题本身移开，因为解决方案在其他地方。

4.有些情绪是对立的，比如：你几乎没办法既高兴又痛苦，既兴奋又低落……花时间考虑一下，究竟什么东西，什么事情可能让对方高兴。

当你做成一件事时，说明你的方法用对了，没必要沾沾自喜；当时没有做成一件事时，说明你的方法有问题，也没必要妄自菲薄。

把心思用在解决问题上，而不是反复的自己纠缠上。很多人焦虑，都是和自己较劲，其实你没那么好、也没那么坏，只是缺少了正确的方法。


成年人只能筛选，不能教育
当真相和愿望不符时，大部分人抗拒真相，好的事情自己会照顾自己，而理解和应对不好的东西才是最重要的。你没必要和别人杠，由于知识的贫穷，他只活在自己的世界里，只认自己的那个想法，你很难改变他！

避免麻烦的最大方法之一就是保持简单



> 困难的事，往往是机会所在。
机会往往伪装成困难到来。做困难的事，你才能取得真正意义上的进步。如果一个人在森林里迷路了，他需要做的第一件事是什么？他应该停下来，不是吗？他需要停下来并查看四周。但我们在生活中越是困惑和迷失，我们就越是急冲向前，寻找、请求、索要、乞讨。因此如果我可以建议的话，第一件事就是让内心完全停歇下来。

机会常常被认为是那些有利的条件和境遇，但从竞争的角度看，每一个困难的背后，都是一个重大的机会。因为困难的事情让大众止步，这正是你脱颖而出的机会。

不要再等待时机成熟，一件事如果有六成的把握就要去做，如果你等到有十成把握，那只能是上帝的事了。

当你回首往事，你会发现，那些让你终生难忘的、值得怀念的、人生的重要经历，都集中在那些曾经让你觉得困难的事情上。


## 因果关系
我们所处的世界有各种各样的因果关系，这些原则可以反映出我们这个世界的多元化。我在这里给大家介绍一下我的生活和工作中的一系列原则。

通过我自己的工作和学习、生活经验，我发现生活就像是一个循环周期。你有一个大胆的目标，然后在实现目标的过程中你会碰到问题，会遭遇失败。你要去分析诊断为什么会失败，因为这是人生当中最为重要的一刻。

失败是我们学习的过程本身，你要把所有失败的原因写下来、分析诊断出来，然后调整你做事情的方式，对于我来说这是最重要的，失败让我们能够有机会学习，学习过程使我们不断地提高。所以在失败当中学习是整个生活工作中最为重要的一个步骤。

我们有这样一个公式：痛苦+反思=进步。痛苦代表你所犯的错误，你需要有反思痛苦的能力，然后需要花一点时间诊断你的痛苦究竟是怎么来的，你的痛苦加上你不断的反思，最后会不断成为你进步的动力。

香港首富李嘉诚曾说到：「成功者与一般人的不同之处，在于面对不好的情况时，成功者往往更快速回复到原本的状态。」

面对痛苦时，那些能够快速回到原有状态，不被情绪所影响者，也都不是一次就知道怎么做，而是在一次又一次的痛苦中，找寻怎么面对，怎么解决，这种反思的过程，就是自我进步的最大关键，因为只有痛苦，少了反思，还是会在感受到痛苦时，感受到相同感觉。

所有的进步，都是甘愿承受痛苦，并且甘之如饴的沉浸在其中。天才与普通人之间的距离，也就在于此。

当你觉得困难的时候，你正在走上坡路

## 迷茫是什么？

迷茫是什么？
迷茫就是：大事干不了，小事不肯干！能力配不上欲望，才华配不上梦想。

焦虑，是因为你把想象中的坏事当真，然后被吓住了，现在开始，停止想象
人活一世，之所以会感到痛苦，是因为心里装错了东西。在这个世界上，没有人能困住你，只有你能困住自己。不活在别人眼里，也别活在自己的情绪里，才是为人处世的大智慧。

如果你感到迷茫，勤奋做事+多读书一定可以解决你的迷茫。如果你想继续迷茫，只需要继续刷手机就好了。

不要试图成为他人，要成为最好的自己，永远不要停止前进。不要和别人比，要和自己比，这就是成长型思维

我读过很多年的古籍和历史，里面有很多人生与故事，印象最深刻的就是环境和命运真的很重要。同样的事情，相同的选择可以有完全不同的结果、相反的却也可以有相似的结果，所以个人的判断是很微不足道的，要习惯把自己交给命运，要善于把自己托付给信仰和信念，有一个适应巨复杂体系的世界观，但也要找到简单而坚定的信仰或人生观、价值观做依托，这样才不容易陷入焦虑、迷惑与自责之中。

如果你发现，大家都对你客气了，不是因为大家素质提高了，而是因为你变强了。在这个世界上，从不缺丛林法则，缺的是公正。

心软和不好意思，只会杀死自己。理性的薄情和无情才是生存利器。

当你全心全意为他人解决问题时，你成长的速度会比那些只关注自己利益的人快好几倍。

“塑造者”=“理想家”+“务实思考者”+“坚毅者”
能提出独特和有价值的愿景，并以美妙的方式实现愿景（通常在他人的质疑下）。塑造者既能看到全局，也能看到细节，塑造者=理想家+务实思考者+坚毅者。

塑造者有一些共同的特征：

-极富好奇心；
-有把事情弄清楚的强烈冲动；
-近乎叛逆的独立思考；
-务实并坚毅地排除万难、实现目标；
-了解自己和其他人的长处和短处；
-能协调团队来实现目标；
-他们能同时持有相互冲突的想法，并从不同角度来看待这些想法；
-喜欢和其他聪明人一起探索；
-能在全局和细节之间自如的跳跃，并相信二者同样重要。

天下难事必作于易，天下大事必作于细
“做那些别人还没觉察到就该做的工作，办那些还没发生事故之前就该办的事，体味那些没有散发出气味之前的气味。

要把小的征兆当成大事，把少的征兆当成多的后果。用恩德对待他人的怨恨。解决难事要从还容易解决时去谋划，做大事要从细小处做起。天下的难事都是从容易的时候发展起来的，天下的大事都是从细小的地方一步步形成的。

因此圣人始终不直接去做大事，所以能够成就大的功业。轻易许诺肯定难以兑现，把事看得太容易肯定会遇到太多的困难。因此圣人要把它看得困难一些，所以最终不会遇到困难。”

人一切的痛苦，本质上都是对自己的无能的愤怒。

怀疑与信仰，两者都是必需的。怀疑能把昨天的信仰摧毁，替明天的信仰开路。

也许现在的生活不是你想要的，但却是你自找的，没关系，你不必喜欢自己的全部过去，也不必释怀。你只需要知道，过去的作用是把你带到现在，这就够了

老样子，不争论，不抱怨，不急不慌，不争不抢，做好自己的事，过好自己的日子，又想起了两年前一位行业长者的话——少说话，多做事，坚持做短期不紧急但长期重要的事。

两件阻碍我们自由的事：活在过去和观察他人



职场三原则:不要卖任何你不会买的东西，不要给你不钦佩的人打工，和与你志同道合的人一道工作

只有用金钱支持你的人，才是最认可你的人，真心想帮你的人，从来没有那么多废话——毛姆

做好小事，熬过难事，静成大事
太用力的人走不远，最糟糕的人生，就是晚上思考千条路，早上醒来走老路。《认知觉醒》里说:“我敢打赌，凡是买了一堆书没读、报了一堆课没上、心中有无数欲望的人，几乎没有主动做成过一件事，比如养成早起、跑步、阅读的习惯，练就写作、画画的技能，考个好成绩、有高收入等等。”

刚刚吃了大亏的时候，不要急着马上翻身，因为那时处于一个人判断力最差的状态。要给自己时间先稳定下来，心态平静下来，理智逐渐恢复，再耐心的寻找机会。



看事情不能只看眼前，要看根本，管理者需遵循“要事优先”原则，一次只做一件事，而且是最重要的事。

何为要事？不能根据压力来决定优先次序。那些压力最大、看上去十万火急的事情不一定最重要。真正重要的事情是，比如关于未来的战略思考、将决策落实为行动等等，却并不显得很急迫。如果按照压力来决定优先次序，那么这些真正重要的事情就会缓一缓再说。而所谓的“缓一缓再说”，往往意味着永远不办。

# 运动

研究发现人在经过有氧运动后，大脑会长出新的神经元，这意味着通过运动，人可以在硬件条件上变得更加聪明。

健身、冥想、充足睡眠，这三点对控制情绪非常重要。做好了，可以解决你80%以上的情绪问题

每天健身半小时，年轻10岁，不要运动过量，保持一定细胞激活

运动会让人精神焕发、健康长寿。最新的科学研究显示：锻炼可以通过改变人体的脱氧核糖核酸（DNA）结构让人变得更加年轻。研究者对2400多对双胞胎的生活方式和习惯进行了长期跟踪调查，发现经常锻炼者的细胞内与寿命相关的端粒，比久坐的同胞长很多。研究显示，每天锻炼半个小时的人要比不爱动的人看上去年轻10岁。

预防疲劳
美军行军每50分钟强制休息10分钟。 煤炭工人挖煤40分钟强制休息20分钟。

高roi(高投产比）的事

运动
早睡早起
喝水
晒太阳

看书阅读
正念冥想
每日拉伸
适度用眼
定期体检
规划日程
设定目标
擦身体乳
学一门技能
写复盘日记
按优先级做事记账和财务规划


# 经济消费

除基础生存外，所有的商品几乎解决的都是精神问题

- 解决的心里问题
- 商品的选择本质竞品的心智斗争
- 行为和决定可能由快思考无意决定

> 注意力>时间>金钱

互联网上最贵的东西有两样：其一是注意力，其二则是情绪。 如果你能妥善地管理好这两者，那么就是你玩互联网；如果你管理不好，那就纯剩互联网玩你了。

当你看到铺天盖地都是这个新闻的时候，你就要想想这是不是别人故意让你看到这些的了，为什么要给你看这些，目的是什么

每个人这辈子都在投资，你的时间、精力都是你的资本，效益体现在2个方面：精神效益（是否自豪）、物质效益（是否有钱）。

观点之后，皆是立场。立场之后，皆为利益。

除了继承、婚姻或盗取，只有一种积累投资资本的方法：花的钱少于赚的钱。

人类本能地渴望立竿见影的效果，在快速赚钱上尤其热情。

你应该使消费适应你的收入，而不是调整收入以适应消费
大部分财富消亡都是因为赌（失败的投资）所致，吃喝玩乐花不了多少钱

这个世界最大的公平在于：当一个人的财富大于自己认知的时候，这个社会有100种方法收割你，直到你的认知和财富相匹配为止。

长期看，能够延迟满足的人会活得更好。延迟满足这东西是天生的，这已经在心理学研究中得到了证明。如果你很冲动，不立刻得到满足就不行，我只能祝你好运，估计你这辈子过不好了，我没办法把你扳过来。

经济学的本质
经济学所有的东西都可以扔掉，但是有两样东西无法舍弃——供给、需求

但是到了股市中，一些人有时候就能因为几篇小作文或者PPT把血汗钱全押金去，乐此不疲，实在难以理解！

这个世界上只有少数人真正活过，剩余的人基本在消费商业创造出来的二手人生

在商业领域，最先做到的人有优势。但是，在研究和学术领域，成功往往不属于最先理解的人，而属于理解得最好的人，真正的优势来自于深刻、基础性的见解。

先行者开辟了道路，后来者用更少的计算资源迅速缩小差距，这就存在了先发劣势。-以谦逊和好奇的态度对待工作时，你会学到更多，参与得更充分。初学者的心态是一种财富。保持真实的自我有助于你找到自己真正的使命。

## 收割智商税

每次去寺庙，你拜的不是佛，而是你自己的欲望

普通人的大脑没有接受过专业训练，生来就是被玩弄的命运

几乎所有能够改变一些人阶级的巨大机会，都被这几样围追堵截： 世俗的眼光，国家的法律，喧闹的舆论

这世界上的事凡是禁止的，往往都是有好处但不想分给你的，凡是提倡的往往是有坑，需要你去填的

散户很难战胜机构，这个道理毕竟经过上百年血和钱的验证，很难被一场巴黎公社式的胜利而改变。

经济学不过是娱乐大众的东西，就像综艺节目一样，从来没有看到经济学家能在股市中大赚

被收割的，其实永远都是同一批人：他们几乎全部的信息都被过滤、重组和屏蔽，且甘愿被屏蔽，且甘之如饴，深信不疑。他们没有善恶之分，没有是非之分，没有常识，没有逻辑。他们敏感且狭隘，玻璃心加巨婴症。

被收割的，其实永远都是同一批人：他们几乎全部的信息都被过滤、重组和屏蔽，且甘愿被屏蔽，且甘之如饴，深信不疑。他们没有善恶之分，没有是非之分，没有常识，没有逻辑。他们敏感且狭隘，玻璃心加巨婴症。他们如同木偶，所有的行动指南，仅仅是18岁之前课堂上读的书本上怎么说，上面怎么说。他们会去攻击韩红，攻击方方，攻击那些其实是在为他们抱薪的人。他们会前一刻还在惊恐万分，后一刻就感动到热泪盈眶。他们会坚持买彩票，坚持买那些扇贝游走又游回的公司股票。他们坚信病毒是美国人的阴谋，坚信中医才是真正的科学，坚信落后就要挨打……

而如果你在朋友圈普及常识或者警示风险，他不仅不会感谢你的好意，还会认为你这是在羞辱他们的价值观与智商而仇视你！

印度第一位诺贝尔经济学奖获得者阿马蒂亚·森在其名著《集体选择和社会福利》里，如是评述：“无数的可怜人，长期活在单一的信息里，而且是一种完全被扭曲，颠倒的信息，这是导致他们愚昧且自信的最大原因。原谅他们吧，因为他们的确不知道真相。”

世界上只有1%的人明白真相，剩下99%的人三观是被塑造的，他们只负责站队，越来越意识到，别说预测未来会发生什么了，99%的人连过去到底发生了什么都搞不清楚。最重要的不是预测未来，而是认识到未来无法预测但可以先做好准备

小资金越想暴富，越会亏光。
这是因为你是小资金，这基本说明你不优秀。

你若具备股市暴富的水平，现实中，你必然早就暴富了，要么是人中龙凤，年薪至少50万+，怎么可能还是小资金?

现实中你都没攒下大资金，到股市却突然长了能耐，可能吗？当别的投资者都是比你同事还傻的傻A_C?

你在职场都唯唯诺诺，受同事排挤，在股市就突然碾压这帮傻A_C了?

所以说，玩了半天牌不知道谁是傻A_C，你就是傻A_C。

普通人的大脑没有接受过专业训练，生来就是被玩弄的命运

他们的要求只有骗子才能满足！
每次去寺庙，你拜的不是佛，而是你自己的欲望

食材越加工，造假概率越大

世间有一种最通行的迷信，叫做服从多数的迷信 ,人都以为多数人的公论总是不错的，很多看似荒诞的东西，其实是有逻辑的。只是，逻辑被隐藏了起来，或者，你看不到。

## 穷富区别

三个贫穷的特质：缺乏计划性、惧怕冒险和缺乏认知

通常来说，打工都打不好的，创业死得更快。创业是资源变现，你要先有资源（认知、信息、资金、经验），才有变现。年轻人自身拥有的资源很少，先积累是正经事，等积累的差不多了，再去考虑创业。

我们要在能力圈里赚钱，在能力圈以外学习

经济学不过是娱乐大众的东西，就像综艺节目一样，从来没有看到经济学家能在股市中大赚

人是很容易线性外推的，好的时候预期更好，差的时候预期更差

每个人这辈子都在投资，你的时间、精力都是你的资本，效益体现在2个方面：精神效益（是否自豪）、物质效益（是否有钱）。

资产
在太平盛世：富人和中产阶级最大的区别，就是富人的东西可以传承，比如稀缺的地皮房产、稀缺好公司独一无二的股权等资产，而上层有各种各样的特殊极度稀缺的核心资源。

而中产的各种头衔职称，主任、院长、教授、科学家，工程师或者一些特殊的技术尤其偏艺术创造性的等，都是有时间限制，过期就作废了，后代得从零开始，重新练级。

想清楚这个了，就应该理解了，要把自己的人生，看成一个公司，经营自己的资产负债表，所有的事业、工作、副业，都是表象，都是用来挣取现金流的，哪些是负债，哪些是投资性支出消费，哪些是税收！哪些是好的生产资料！

> 穷人沉迷多巴胺，富人追求内啡肽

如何让穷人安分守己？他们想出了一个办法：
只需要像喂婴儿奶嘴一样，为穷人提供源源不断的娱乐就行了。
这就是著名的“奶头乐理论”。

越是富人越是精英阶层，他们的闲暇娱乐，越喜欢采用补充型方式。

比如阅读、学习、运动。

而越是底层的穷人，他们越喜欢采用消耗型方式，为自己提供快乐。

比如打麻将、玩游戏、看肥皂剧。

多巴胺的爽感太廉价了。如果你沉迷在低级快乐之中，再多的财富，也会在一次次的舒适里消耗殆尽。

内啡肽是痛苦后的补偿，需要你费尽心血才能得到，却能为你带来脱胎换骨的改变。

金融的神奇之处在于，可以从一无所有的人身上继续拿走东西

未经思考的努力，才是我们贫穷的根源

## 骆驼传奇
骆驼成功穿越沙漠，成为野外生存专家， 驴子很羡慕，就虚心向骆驼求教成功秘诀。

骆驼讲了很多野外求生方面的技能，又强调说坚持和努力更为重要。学完以后，驴子信心满满上路了，这一去，再也没回来。

其实，骆驼教给驴子的那些技能都对，但唯独没告诉驴子，它有驼峰。

所有的成功秘诀都是很难复制的，也不会外传。就像那些曾经成功的大佬，从来不会宣传自己的家庭背景。

ai带来知识平权 信息平权 科技平权