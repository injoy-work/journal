## 短剧剧本- 无双


第一集

**1-1 夜 外 街边摊**

**人物：楚青 姜萤（机车性感装扮） 周麒 打手3名**（**周麒第一批，打手1有词） 打手5名**（**周麒第二批） 食客路人6** **摊主**

▲姜萤骑机车至摊边刹停，摘下头盔，扫过人群，视线最终定格在楚青身上，勾起唇角，下车上前，径直坐到楚青对面。

**姜萤**：拼个桌，不介意吧？

▲楚青没说话，默默将碗向自己挪了挪，腾出空，姜萤笑笑，正要说些什么，三名打手持器械走来，重重砸在桌上。

**打手1**（凶戾）：从现在开始，这一片，清场！

▲客人和摊主都畏惧逃散，姜萤皱眉，楚青继续吃。

**打手1**（阴沉扫向楚青姜萤）：你们两个，还不滚？

**姜萤**：空位这么多，凭什么你们一来就要清场？

**打手1**（冷笑）：小妹妹，你听说过森爷的名号吗？

**姜萤**（微微皱眉）：南省坐山虎，任城森？

**打手1**：不错！今晚，森爷亲至海城，他老人家念旧，惦记这个档口，所以我们大哥要在这里宴请森爷！而你们，算什么东西，也配和森爷并坐？

▲打手1狞笑，抬起器械砸在桌上（桌子上的饭全部被打翻，桌子也翻）。

**打手1**：识相，就赶紧滚。不然，这就是下场（扬起器械，狠狠砸翻餐桌）！

▲姜莹被突然的重击惊吓起身退开到一边（出画离场），楚青阴沉抬头看向打手1

**楚青**：你，砸了我的饭。

**打手1**：怎么？你有意见？

**楚青**：赔！

**打手1**（愣了愣，哼笑，指指自己，指指楚青）：你让我，赔你的饭？

**楚青**：对。

▲打手1扭头和另外两名打手对视，爆发哄笑。

**打手1**：行，没问题。老子再给你加点码，废了你，一块儿赔！给我弄他！

打戏设计：两个小弟上前，一个持器械向坐着的男主砸去，男主连同闪避加拾起地上的酒瓶向打手2的头，打手2倒地，男主起身将打手3正面踹飞，重摔在一张桌子上，桌子损坏，打手3倒地不起。

▲打手一惊，接器械砸向楚青，楚青反手卸下器械，锁住打手1的胳膊。

**打手1**（惊）：你？

**楚青**：这条胳膊，别要了。

**打手1**（无法挣脱，阴沉）：你敢！我大哥，可是森爷亲指的海城地下魁首！你动了我，活不过今晚！

**楚青**（笑笑）：那我倒想看看，他怎么让我活不过今晚。

**画外音**（**周麒**）：好大的口气！

▲楚青侧目，周麒率4名打手赶来，阴沉望向楚青。

**周麒**（寒声命令）：放开他。

**打手1**（喜）：我大哥到了！你完了！赶紧放开我！

**楚青**（笑笑）：好啊。

**打手1**（得意）：算你识相……

▲话未完，楚青拧断打手1胳膊，打手1哀嚎倒地，楚青微笑望向周麒。

**楚青**（无辜抬手）：我，放开了。