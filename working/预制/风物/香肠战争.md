
- 香肠被抢，反而做一顿给我吃
- 让我帮忙做作业，被我骗吃
- 拉架被我打了一顿
- 结果被竹插穿了身子，

原本以为来到省城上学就会平平安安上学，开开心心回家，那里知道有同学的地方就有江湖，这里的收陀地，有时候是2文钱，有时候是麦芽糖，有时候是半条甘蔗。

早上把两文钱藏在鞭子里躲过了混混劫道，我在学校门口买炸串，看见黄孩儿要来，带着小混混走过来。我心崴了，手执的两文钱，赶紧跟卖大娘要了2串香肠，“香肠”这种食物在当时属于新式菜品，只在大省城里才有。

不好了， 我趁着他脚踏流星的速度，迅速伸出了三尺之舌，以脚步不及舌头之舔动，就在他靠近我身上一尺，我把肠子从头吮到脚挨个都舔了一遍，提前完成了KPI。

结果他根本不嫌弃我，一手抢过我的鱼蛋扬长而去，露出慈父般的微笑，还丢了一句送给我，大丈夫成大事必须不拘小节。我心里凉了！不用说，我的脸不是象块砖头，就是象个黑蛋。不过还是在我安慰一番？毕竟吃了我的口水沫，好歹见我一声爹，可能我放荡不羁的眼神烫到了他，灵感一到，抢过我的水葫芦，开始帮肠子洗刷刷，还崩出了一句，快给肠子洗个澡咯，扔下水葫芦，口里叼着香肠闪了。

想起来我那损哥来了，小时候看他吃着冰棍，我就去要，结果他把冰棍360℃无死角舔了一遍，恶心的我没敢吃，次日我也吃上冰棍，他来要便效仿他，以为他也不敢吃，谁想到他拿到水管底下冲了一下，冰棍小了一圈。被他拿走吃了

天杀的，连人口水沫子都不放过，那水葫芦的水我喝过的~如此又蠢又懵的，那两文钱是是省了一周的早餐钱才拿到的。

## 报仇

这事情让我很震惊，也很受伤。于是放假过年一轮暴饮暴食，或者很我长得很着急，高中的时候脸上已经有一圈胡子。

袁崇焕心想：“这毛文龙怎么这么不可理喻呢？”便又换了一个话题说：“我要给你部所有官兵进行赏赐，你现在把全体官兵的花名册交给我。”要赏就赏嘛，赏了之后让人家自己去分，还要人家的花名册，这个花名册其实是袁崇焕的私心，他要核查毛文龙部的实际人数。毛文龙对这个问题相当敏感，连忙搪塞道：“我从皮岛带来的士兵，加上双岛的驻军，一共三千五百多，明天一起来领赏。”

毛文龙拒绝交出全体官兵的花名册，等于不打自招，可是，袁崇焕没有选择继续追究毛文龙冒领军饷的问题。因为，这不是袁崇焕此次会见毛文龙的目的，他要按计划行事，不想打草惊蛇。dd

第三天，袁崇焕在双岛上犒赏毛文龙手下的三千五百名官兵。犒赏完毕，毛文龙进入袁崇焕的大帐当面道谢。袁崇焕问道：“你手下的各位军官，为什么不一起来见我？”袁崇焕上双岛以来，每次都只是与毛文龙单独见面，这一次却突然要见所有军官。这叫擒贼先擒王，将军官全部控制起来之后，那三千五百个士兵便是一群乌合之众。毛文龙根本没有想到这一层，连忙叫自己的手下进帐拜见袁崇焕。

毛文龙手下的众位军官走进大帐之后，袁崇焕一个一个地问他们的姓名，不料他们个个都姓毛，什么毛可公、毛可侯、毛可将、毛可相。一个部队里的军官不可能都姓毛，这显然是毛文龙给起的。毛文龙连忙在一旁解释说：“这些都是我的小孙子。”别解释了，越描越黑。全军将士都姓毛，显然这已经不是朝廷的军队，而是毛家军。毛文龙已经沦为一个割据一方的军阀。

面对一群毛文龙的“小孙子”，袁崇焕面色一沉，当面揭破毛文龙的谎言说：“你们哪里都姓毛，不过是出于不得已。”接着袁崇焕又和毛文龙的部下套近乎。袁崇焕说：“像你们这样的好汉，个个都可以重用。现在宁远前线，我手下将军挣那么多钱，士兵发那么多口粮，可是他们还是吃不饱。你们在这荒凉的海岛上，这么辛苦，每个月那么一点儿军饷，许多人还要养家糊口。你们太辛苦了，处境太让我感动。在此受我一拜。”话音未落，袁崇焕恭恭敬敬地对众将官施礼。袁崇焕显然在撒谎，他手下的士兵为拖欠军饷闹兵变，这才刚平息几天啊！

袁崇焕施完礼之后，又当着毛文龙的面对他的部下说：“如果你们愿意为国家出力，今后就不会再为军饷发愁了！”言下之意，这帮人都不是为国家出力，而是在为毛文龙个人干活。袁崇焕这是在收编毛家军，当着毛文龙的面。袁崇焕也不怕，因为他太了解这些雇佣兵，知道军饷对他们的诱惑力。果真，毛文龙的手下被感动得连连叩头谢恩。

毛文龙见自己的手下一个个都这副德行，心想：“这些人都怎么了？为了几个军饷，至于吗？”还没有等毛文龙想明白呢，袁崇焕突然对毛文龙大声喝道：“毛文龙你可知罪？”毛文龙毫无思想准备，一脸的茫然，嘴里下意识地回答：“知罪？知什么罪？”袁崇焕立刻当众宣布了毛文龙的十二条该杀之罪。

毛文龙听完袁崇焕列举的十二条罪状后，一脸漠不关心的神情。他心想：“这区区罪状，就想要整我？如果这些所谓罪状就足以构成死罪，那么整个朝廷和朝内朝外的官员都是死罪了。”显然，这十二条罪状无法震慑毛文龙。

袁崇焕看到毛文龙的不屑和不服气的表情，心中十分愤怒：“这个人狂妄自大，以为我无法制裁他？”想到这里，袁崇焕毫不犹豫地拿出尚方剑，面向北京城的方向，以崇祯皇帝的口吻，厉声喊道：“除掉毛文龙的官服，绑起他来！”袁崇焕的手下立即冲上去，抓住毛文龙并开始捆绑他。毛文龙奋力挣扎，并大声喊道：“我没有罪，凭什么要抓我？”

袁崇焕大声地斥责毛文龙说：“你没罪？你带兵截杀难民，然后冒充是敌军的士兵，向朝廷领赏，你这是‘杀良冒功’! ”毛文龙大喊冤枉。袁崇焕接着训斥道：“冤枉？那你两次带兵到山东登、莱二州抢掠钱粮，并且口出狂言：‘牧马登州，取南京如反掌’，这也是冤枉你不成？这两条哪条都构成你的死罪！”

这两条罪状还真是有点分量，毛文龙不再嘴硬了。可是，这会儿在毛文龙的地盘上，当着毛文龙众部下的面，帐外还有三千五百名毛文龙手下的士兵，袁崇焕如何才能下手杀了毛文龙呢？

袁崇焕手捧尚方剑说：“今天我将你处决，如果未来我不能收复辽东，我愿意以这把尚方剑偿还你的性命！”这番话实在缺乏逻辑，为什么这样说呢？因为，毛文龙虽然犯有罪行，但是否杀他与收复辽东没有任何关联。袁崇焕非要把这两件事联系在一起，显然是想用收复辽东这个大事来压制毛文龙。好像只有杀了毛文龙，才能收复辽东；不杀毛文龙，就像永远无法收复辽东一样。

更不讲道理的是，袁崇焕转过身对毛文龙的部下说：“毛文龙犯下如此严重的罪行，你们认为我应该处决他吗？”这种提问，让这些将领怎么回答呢？袁崇焕心知肚明他们不会说话，于是接着说：“如果你们认为我冤枉了毛文龙，那么你们就来杀我吧！”说完，他将尚方剑架在自己的脖子上。袁崇焕这是何意？他是钦差大臣，谁敢杀他呢？而且此时，他随身的参将和卫士已经控制住了毛文龙的部将，他们根本没有反抗的机会。袁崇焕无非是想让毛文龙的部下明白，他不仅要在他们面前处决他们的大将，还要让他们表示赞同。这实在是太过分了！毛文龙的部下被袁崇焕逼得无可奈何，只好跪地叩头求饶。

袁崇焕此时觉得杀毛文龙的时机成熟了，便大声宣布说：“我要在五年之内平定辽东，就必须依法行事。毛文龙犯下这么大的罪，今天如果不斩他，以后让我如何管束别人呢？这就是皇上赐给我尚方剑的原因。”袁崇焕要杀毛文龙，真的是“依法行事”吗？根本不是那么回事！明朝的法律很严格，像毛文龙这样挂将军印的左都督，即使是钦差大臣，手握尚方剑也没权力杀。

毛文龙一见袁崇焕杀心已定，只好屈服了，跪在地上连连求饶：“文龙自知死罪，只求恩赦。”袁崇焕当然不肯饶了毛文龙。他说：“你长期目无国法，如果不杀你，东江这块土地，就不属于皇上所有了！”袁崇焕说完这话之后，向京城方向跪倒叩头，如同面对皇帝请旨一样：“臣今天杀毛文龙就为了整肃军政，臣如果五年不能收复辽东，请求皇上就像今天杀毛文龙那样，杀了微臣。”说完这番话，袁崇焕起身将尚方剑交给旗牌官，旗牌官立刻将毛文龙推出大帐给斩了。

不限制穿着。既然穿不穿校服无所谓，大家也自然都不穿校服来上课了。跟所有高中一样，混混大哥们经常在厕所喷云吐雾，商议社团事务。这是背景。

一天，我课间尿急去上厕所。进了厕所门，只见黄孩儿突然慌张起来，迅速把烟掐灭扔到身后。再加上当时体重超标，满脸横肉，经常在各种场合被各种人高估年龄。每每被同龄人叫大哥，被妹子们叫叔叔，被长辈叫同志，就很难受。

林冲豹头环眼，这可是张飞的造型啊！说明啥？说明豹子头本不是善茬！

可就是这样一个张飞一样的豹子头，为了生活、为了生存，忍气吞声到了什么地步？？

老施这样写，就是要告诉我们，就算你暴躁勇猛如张飞，只要你生活在大宋，只要你想正常的生存，遇到了高衙内，也只能忍气吞声，”豹子头”也得”抱着头”！

多么痛的领悟！ 因为林冲头上的纹身 每次林冲发怒或兴奋，头上纹身便愈加显眼 于是众人高呼 “豹子、豹子、豹子” “666，逢赌必赢”

如果你认为豹子头林冲是豹头环眼，那不是你穿越了就是我穿越了。我这边豹子头林冲是头上纹了666

古代储存蔬菜是个难题，如下为明代的蒸干菜法: 将绿叶蔬菜洗干净，用沸水煮五六分熟，然后晒干。 将盐，酱油，莳萝（可用茴香代替），花椒，砂糖橘皮，再与晒干的菜一同煮熟，晒干。 吃的时候可以加点醋香油蒸熟拌饭吃。没菜吃的季节拿出来补充维生素。 很有深意的称号！

## 拉架打人

把一个咸猪手门卫打进了医院。

这个门卫是个老痞子，我们高中有一阵子抽风，早上进校门要检查仪容仪表，还要查胸卡什么的，这个老痞子就趁这机会揩油。

除了对女学生，对年轻女老师也下手，年轻女老师去门卫取个东西什么的他就要找机会占便宜，导致后来年轻女老师都不敢去门卫室取东西，都得叫我们这些小伙子去帮忙取。

后来有一天，这老痞子占便宜占到我们学校一混混的女朋友身上了。然后被这哥们抓住当场暴打，当时校门口其实还有值班的女老师，但估计也是被这老痞子占过便宜的，所以没有第一时间上去制止，而是等老痞子挨了一会揍后，才叫其他男同学上去拉架（在下就是当年上去拉架的其中一人，我们几个拉架的很默契的拉了偏手，光摁门卫，不摁混混）。

最后是几个校领导带着体育老师过来了，混混才罢手，这时候那痞子门卫已经被打的站不起来了（去医院检查，是肋骨骨裂，后来好了也没敢再回来上班了）。

至于混混嘛，听说因为很多老师说情，也没给太大处分，就给了个全校通报就完事了。 而且因为这件事，仪容仪表检查彻底取消，胸卡什么的更是不用查了。

## 枪手

土豪要附庸风雅，准备掏钱出版一部八股文选本。

可惜，他本人没什么名气，便跑到南京城，想找名士帮自己托底，于是一头撞进了的刻字店。季恬逸那时已经落魄得连“吊桶底”大饼也吃不起，快要饿死了，忽见天上掉下一个救星，喜出望外。

他在大街上逮住同乡我，假充名士，两人合伙，决定把诸葛天申当成“猪头”，狠狠宰一刀。

三人在报恩寺选定一处寓所当工作室，安顿下来之后，已到掌灯时分，晚饭时间也过了，直接吃宵夜——

三人点起灯来，打点夜消。
诸葛天申称出钱把银子，托季恬逸出去买酒菜。
季恬逸出去了一会，带着一个走堂的，捧着四壶酒、四个碟子来：一碟香肠、一碟盐水虾、一碟水鸡腿、一碟海蛰，摆在桌上。

诸葛天申是乡里人，认不的香肠，说道：“这是甚么东西？好像猪鸟。”诸葛天申土包子进城，不认识香肠，呼为“猪鸟”，此处有和谐，按照康熙字典和《水浒传》的读法，发音为diro。

（当年第一次看到此处，便留下深刻印象，导致后来每次见到香肠，都会想起这个比喻）

而相识才半天的我早已看穿眼前这家伙，是个没见过世面的乡下土豪，根本就懒得解释——

我道：“你只吃罢了，不要问他。”

诸葛天申只好在自己的认知范围内尽力揣测这种新式食品——

诸葛天申吃着，说道：“这就是腊肉！”

我决定耍他一把——

我道：“你又来了！腊肉有皮长在一转的？这是猪肚内的小肠！”

萧、季两人，住着诸葛天申租的房子、吃着他花钱买的酒菜，还如此捉弄他，实在不堪。

### 让他去买菜

《齐民要术》卷九 说回“香肠”。

这种食物在古代文献里极少出现，清代之前几乎没有记载。《儒林外史》早在乾隆十四年已有抄本。

有一种说法，认为北魏贾思勰在《齐民要术》卷九中提到的“灌肠法”，就是香肠的起源。

取羊盘肠，净洗治。
细剉羊肉，令如笼肉，细切葱白，盐、豉汁、姜、椒末调和，令咸淡适口，以灌肠两条。
夹而炙之，割食甚香美。

这讲的是在羊肠内灌入切细、调味的羊肉，用火烤熟之后，切开食用。

个人以为和吴敬梓笔下写到的香肠还是有区别。

首先，《儒林外史》里提到的香肠显然是猪肉制成的，不是羊肉。

其次，它既然吃起来像腊肉，那就是经过风干的结果，而不是如贾氏所描述的，灌完之后直接用火烤炙，马上就能吃。

最后，小说特别强调，从小县城来的没见过香肠，可见它在当时并不普及，是大城市才有的美食，以至于被用来体现他是个很好骗的“乡巴佬”。从北魏就开始流传的“灌肠”。

## 结局

光顾着吃香肠，不小心看到竹竿，中午一点多返回，黄孩儿犯困，撞上了路上的栏杆，车速很快，栏杆直接插进中巴车里，靠车的几个混混小弟的几个像串羊肉串一样串着。（原话，没有加工）

所以他现在都不敢开车，也不敢坐高速车，出门都坐火车。