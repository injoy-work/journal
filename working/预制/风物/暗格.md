暗格

- 墨水和纸能制作假画
- 

和大胡子赌博 拔胡子

癖好 拔胡子蚂蚁搬动胡子发现暗隔

治小人犹豫烹小鲜

墨的主要成分包括两部分，一部分是烟炱，一部分是胶。其中并没有粮食的成分。

墨所用的烟炱是焚烧松枝或者油脂形成的，一般是烧窑焚烧这些含碳和油脂的物质，焚烧后在窑壁上，会集结这样的烟炱。烟炱听起来生僻，但是在乡村的烟囱中，以及在你烧菜的锅底上，都会凝结烟炱。

烟炱是极细微的颗粒。直径只有 0.5μm，其中有超过 50%的成分是碳黑。烟炱亲油厌水。

桐油炼烟的方法，没有亲眼见到，想必非常壮观浪漫。

一般是用桐油，碗里放根灯芯点着后，火苗上方再扣一个碗，桐油燃烧后的碳就附着在碗里。一个大房间，几百盏桐油灯在熏碳，很是壮观。

烟炱是人类日常可以找到的非常黑的颜料了。但是调和烟炱粉末到水中用于书写，却有难度。因为它并不亲水。不怎么溶解。所以中国人采用了加胶的方式来制成稳定的颜料。

这种胶，一般是动物胶。猪皮、牛皮、鱼鳔、驴皮之类，都可以熬制出胶。动物皮和水加热熬制，去除渣滓，就成为非常好的胶。这个胶充分熬煮脱去水分，冷却后是非常坚硬的团块。

将它加热后，就成为粘稠柔软的团块。

按照一定比例，把烟炱和热胶混合起来，混合均匀，冷却，就成了墨块。但是为了保证混合均匀，方法不是如我们揉面一样搅拌按揉，而是通过反复折叠捶打，将烟炱和胶充分混合。

据说上万次的混合，才能让墨充分均匀。

由于动物胶本身具有一定养分（就是胶原蛋白嘛）在潮湿的环境下这个胶会腐坏，就产生一种独有的臭气。这就是墨臭。质量不好、价格低廉的墨块或者墨汁，就常有这种臭气。廉价墨汁写字，纸上就会有一种臭味，要干了之后才能消除。墨臭来自胶而不是来自烟炱。烟囱中并不会有太难闻的气味。

为了去除或者覆盖这种臭气，名贵的墨制作过程中，要加上香料和防腐药物。比如麝香冰片之类。这样墨就有了香气。

刚刚锤炼好的墨，仍然是柔软的，和温热的饴糖差不多。把他们放到不同的模具中，就可以压制成不同的形状。包括表面花纹，都是这么来的。并不是雕刻的产物。
