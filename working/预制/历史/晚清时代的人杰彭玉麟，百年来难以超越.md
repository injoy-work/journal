晚清时代的人杰彭玉麟，百年来难以超越

 
1861年，45岁的彭玉麟做出了一个重要决定。

当时，他正率领湘军水师在武汉与太平军作战。一道升官谕旨急速送到前线，皇帝要他出任安徽巡抚。这是天大的好事，帝国无数官僚苦熬一生，均远未抵达这个位置。要知道，整个晚清70年，封疆大吏（含总督、巡抚）才出了370多人。

更难得的是，彭玉麟仅读过县学，没有科举功名。以如此低微的“学历”获任一省之长，按老辈人的说法，叫祖坟冒青烟了。但彭玉麟竟然毫不心动。人家还来不及跟他道贺，他却决定要辞官。

曾国藩劝他不要辞，劝他为家族着想，为君王着想。
彭玉麟根本不听，连连摆手：干不了，谢谢。

他真的上疏辞官，一次不行，再辞一次。根本不像官场惯例所做的那样，客套一下，做做样子，显示谦虚的美德，然后就可以美滋滋地赴任了。他极其认真地陈述了自己辞官的理由。主要有三点：

一、自己只读了县学就从军打仗，不懂刑名（法律）与钱谷（经济），缺乏做巡抚的文才，唯恐误国误民；

二、从军以来，一直率水师作战，这是自己的长处，当此国家用兵之际，舍水师而做巡抚，将是国家的损失；

三、自己的性格比较生硬偏激，不懂圆融变通，出任巡抚恐怕无法做到精深稳健。

两道辞官的奏折递上去后，清廷深受感动，认为彭玉麟所奏确实“真实不欺”。不过，人才难得，清廷并不想轻易“放过”他，于是给曾国藩寄了道谕旨，说彭玉麟随同你作战这么多年，你对他最了解，你说说他到底能不能胜任安徽巡抚一职。

明眼人都看得出，这表面是询问，实际上是要曾国藩再做彭玉麟的思想工作。凭曾国藩费尽口舌，彭玉麟就是油盐不进。

最后曾国藩也没办法，只能以水师离不开彭玉麟为由，上奏替他辞官。彭玉麟自己则“趁热打铁”，又上疏辞了一次。这样，彭玉麟三辞，曾国藩替他一辞，总共辞官四次，清廷这才允准，暂时断了要他做封疆大吏的念想。

 
辞去安徽巡抚，仅是彭玉麟在帝国层面“辞官生涯”的开端。终其一生，他都在辞官。

关于他辞官的次数，历来说法不一。历史学者李志茗经过统计认为，彭玉麟实际辞官8次，请求开缺回籍9次，请求开除差使职务6次，一共达23次。

我们知道，彭玉麟辞官的起点就是巡抚，这相当于现在的官员放着省长不干，接下来辞官的品级绝对不可能低于省部级。一起来看看他主要辞过哪些官职吧。

1864年，辞任漕运总督。

当年，湘军打败太平军，清廷对湘军将领论功行赏，封官加爵。彭玉麟的水师与曾国荃的陆军，并称湘军的左膀右臂，居功甚伟。彭玉麟因此获任漕运总督，一个大肥缺。别人看着眼红，他却闭着眼睛就把官辞了。清廷不同意，催促他赴任。

他急了，索性上疏要求把自己那个兵部侍郎的虚衔也免掉，说自己当年投军是为国效命，现在太平军已灭，是时候解甲归田了，如果还想着好官我自为之，那就是贪位恋权。朝廷没办法，只好准许他回籍修养，而后仍然负责巡视长江水师。1881年，辞任两江总督兼南洋通商大臣。

当年，两江总督刘坤一被召入京，朝廷第一个想到的顶替人选就是彭玉麟。两江总督在帝国封疆大吏中的地位之显赫，仅次于直隶总督。

彭玉麟深知朝廷对自己的看重，但他依然心静如水，上奏推辞。理由写了很多，包括说自己是一介武夫、不善理财、不善洋务等等。辞了两次，最后说自己“愿以寒士始，愿以寒士终”。朝廷拗不过这个倔强的官场另类，准了，改由左宗棠充任。左宗棠毫不客气，走马上任。1883年，辞任兵部尚书。

这一年，彭玉麟已经67岁。朝廷面对这样一位功勋卓著而又无欲无求的宿将，总觉得对他不住，他所任的职务全是虚职，实在是委屈了。就算要退休，火箭提拔一下，好歹留个退休待遇，怎么也不为过。于是朝廷又主动给年迈的彭玉麟授官，提拔他为兵部尚书。

但彭玉麟还是不领情，仍像前几次一样上疏力辞，强调自己年事已高，希望能够辞官返乡养老。就在此时，帝国南疆传来战事消息，彭玉麟突然态度完全转变，说不辞了，兵部尚书给我吧。原来，中法战争爆发了，老将又要上场了。

 
战事紧急，朝廷本来命李鸿章率军赴广东督战。但李鸿章不愿意去，连上几道奏折拒绝赴任，还跟人抱怨说，朝廷竟然要他“白头戍边”，甚为寒心。清廷无奈，改派他暂驻上海，统筹全局。

外人挑衅，而偌大的帝国竟无人可用吗？朝廷最后还是想到了彭玉麟，要他以兵部尚书的身份前往广东督战，并要求他迅速启程。

彭玉麟一边在辞官，一边接到赴粤督师的任命，他当即动身，毫不推辞：“今广东防务吃紧，时事艰难，朝廷宵旰忧勤。臣一息尚存，断不敢因病推诿，遵即力疾遄征，以身报国，毕臣素志。”

比彭玉麟年轻了六七岁的李鸿章，不愿“白头戍边”，而彭玉麟一听边疆危急，奉诏即行，以身报国，全然不顾自己年老一身病。

帝国末世，那个时代最一流的人才，每一个都干得很不容易。只是，做决定的时候，有的人考虑自己多一些，有的人考虑国家多一些。

彭玉麟的决定，让帝国有良知的官员均深受感动。以前，他屡辞高官的时候，官场中人难免蜚短流长，什么样的风评都有：

有的说他自命清高，沽名钓誉，因而弹劾他辞官不到任是“抗诏”，是自居功臣，骄矜狂妄；
有的说他自保意识很强，看似憨直，实则情商颇高，时刻懂得激流勇退，保命要紧；
有的说他以退为进，是想捞取更大的政治资本，他越辞官，朝廷就越信任他，越给他更高的官职，你看他得逞了吧。

官场之中，人与人最大的差距是什么？

最大的差距是，有的人一生追着官位跑，有的人一生被官位追着跑。

而这两种人的数量并不对等，前者多如牛毛，后者凤毛麟角。彭玉麟生性不喜追逐名利与官位，这样的思想境界，放在任何时代都是高山流水，曲高和寡。真正懂他的人，太少太少。大家更热衷以己之心度人之腹，只是，乌鸦永远见不得白鸟。

即便是同时代第一流的人物，也未必能达到彭玉麟的思想高度。张之洞就曾批评彭玉麟辞官的做法，说他“孤行己意，坚不任职”，“有识之士，不无遗议”。

直到彭玉麟勇任中法战争前线钦差大臣，帝国官员们才相信，这名特立独行的老将，真的是以身许国，无惧无畏，真的是烈士暮年，壮心未已，真的是如他所标悬的那样——不要钱，不要官，不要命。

张之洞也改变了对彭玉麟的看法，从批评到赞赏，从赞赏到敬服。听到彭玉麟奔赴广东，张之洞说，“加官不拜，久骑湖上之驴；奉诏即行，誓翦海中之鳄”，彭玉麟不愧是这样一个矍铄的老头。

很快，清廷任命张之洞为两广总督。张之洞二话不说，走马上任，与彭玉麟共事一方。

后世史家公认，中法战争中，中国在战场上不落下风，尤其是老将冯子材取得镇南关大捷，离不开彭玉麟与张之洞二人同仇敌忾，和衷共济，竭力抗战。

战争结束后，彭玉麟要离开广东，张之洞十分不舍，提出要拜彭玉麟为师。彭玉麟大为震惊，连忙回绝。尽管这段师生关系终未成立，但晚辈张之洞对彭玉麟人品与能力的景仰，可见一斑。

 
彭玉麟一生经历的两大战争，于他而言，均取得胜利。但这一次，他却心情沉郁，难掩痛苦。

中法战争的结局，以清廷接受和议的屈辱方式收场，后来被评论为“中国不败而败，法国不胜而胜”。

彭玉麟无法接受这一胜负颠倒的战果，时任帝师翁同龢在日记中说：“彭（玉麟）电请勿撤兵，先向法索兵费一千万。”清廷不听，和议完成。

本已年老体衰的彭玉麟，经此刺激，身体几乎垮掉了。他又开始上疏辞官。

但清廷始终不让他辞去一个虚职，那就是巡阅长江五省水师差使。

彭玉麟是长江水师的创建者之一，也愿与水师相终始，遂表示“谨遵上谕，照旧巡阅长江……断不敢借病推诿”。他被认为是帝国海军的奠基人，每年都要巡视水师，哪怕抱病，亦不例外。

最后一次出巡，当他抵达安庆时，安徽巡抚陈彝前往迎接。陈彝看到彭玉麟步履蹒跚，老病缠身，请他务必到城里休息。

彭玉麟拒绝了，像以前率领水师训练一样，他坚持与将士一道住在船上。

陈彝特别难过，他上奏朝廷，说彭玉麟确实病得厉害，希望朝廷准许其回乡养病。朝廷这才批准了。

在残烛之年，彭玉麟终于回到老家衡阳，回到他最后的归宿——那座四周种满梅花的退省庵。

回首往事，在他37岁那年，他经不住曾国藩的一再邀约，出山加入湘军水师，独率一营。在太平军的炮火如雨中，他独立船头，只说一句：“今日，我死日也。吾不令将士独死，亦不令怯者独生。”遂一战成名。

日后成长为湘军水师的标志性人物、中国近代海军的奠基人，全凭当年“不要命”。

 
在他出山后第二年，因为率水师攻陷太平军要地，朝廷奖励他4000两白银。他转手就全部用于救济家乡，在给叔父的信中，他说：“想家乡多苦百姓、苦亲戚，正好将此银子行些方便，亦一乐也。”他要求叔父从中拿出一些银两办所学堂，期望为家乡“造就几个人才”。

不曾为子孙留钱，说“钱愈多则患愈大”，如果留钱给了不肖子孙，狂嫖滥赌，挥霍无度，反而害了他们一生。他儿子装修三间老屋，不过是土墙瓦屋，费银无多，他知道后，写信把儿子大骂一通。

他一生崇俭，不要钱，不愧是晚清帝国的一股清流。

加上一生都在辞官，他被称为“三不要”的官场另类：不要钱，不要官，不要命。

人无欲则刚。

他曾一语骂尽追名逐利的士大夫群体，说：“天下之乱，不徒在盗贼之未平，而在士大夫之进无礼，退无义。”

他曾觉察出曾国荃为人不正，建议曾国藩“大义灭亲”，曾国荃为此对他忌恨不已，曾国藩却只能劝弟弟要反躬自省。

回首往事，这名硬汉俯仰无愧，无愧家国，无愧时代，无愧天下。

 
难得的是，在时代的腥风血雨中，这名猛将亦有铁血柔情时。

人们说他“百战归来，一心画梅”，大半生只要有闲暇，就静下来画梅花图，配上梅花诗，据说画了上万幅。所画之梅，亦入化境，被称为“兵家梅花”。

而他最爱的梅花，或许深藏着他一生的愧憾。

据史学家考证，彭玉麟的外婆有个养女，仅比彭玉麟大几岁，两人从小一起长大，感情极深。彭玉麟叫她“梅姑”。因为辈分问题，两人未能结合。后，彭玉麟听说舅舅去世后，外婆和梅姑在安徽孤苦无依，遂派人把她们接到衡阳一起生活。不久，彭玉麟母亲做主，把梅姑嫁出去。四年后，梅姑死于难产。

彭玉麟听闻噩耗，伤心欲绝，开始画梅写诗，以作纪念：

自从一别衡阳后，无限相思寄雪香。
羌笛年年吹塞上，滞人旧梦到潇湘。

他曾用过一枚印章，自称“古今第一痴人”，对这段感情的创伤与痴绝，表露无遗。

他在诗画中无数次表达同一个意思，说“一腔心事托梅花”，说“一生知己是梅花”。他屡屡把梅花当作相依相伴的爱人，说“生平最薄封侯愿，愿与梅花过一生”。

在中国的文化传统中，梅是“四君子”之首，寄寓士人高尚的道德追求。画到后来，以梅怀人与以梅言志，在彭玉麟笔下已融为一体，难解难分：

英雄气概美人风，铁骨冰心有孰同。
守素耐寒知己少，一生惟与雪交融。

1890年，光绪十六年。彭玉麟病逝于衡阳退省庵——四周种满梅花的住所。享年74岁，谥号“刚直”。

他死后，上至缙绅高官，下至贩夫走卒，都难掩涕泣：“彭公逝矣！”他的去世，被史学家称为“大清帝国最后一抹斜阳的消逝”。

这些年，我写过许多历史人物，尤其是那些个时代的悲情英雄，每一个我都能勉力去写完他，算是迟到的致敬。但这次写彭玉麟，写到这里，仍觉得不应该结束，只是心中有话怎么都写不出来。

以我一个俗世的读史者，真的不配评论彭玉麟。

曾国藩识彭玉麟于微时，他看人确实看得准，看到了彭的一生刚直与半世多情，救世担当与隐世情怀。

烈士肝肠名士胆，杀人手段活人心。
——曾国藩评彭玉麟

黄体芳是晚清“翰林四谏”之一，他为彭玉麟写的挽联让人读后无限怅惘：彭玉麟之死，标志着“同治中兴四大名臣”全部告别历史舞台。彭玉麟将与此前先行一步的曾国藩（文正）、左宗棠（文襄）、胡林翼（文忠），相聚于另一个世界。

于要官、要钱、要命中，斩断葛藤，千年试问几人比；
从文正、文襄、文忠后，开先壁垒，三老相逢一笑云。
——黄体芳挽彭玉麟

俞樾是晚清的大学问家，他对彭玉麟的评价最高，说彭是同时代中最完美的人。

咸丰、同治以来诸勋臣中，始终压服人心，无贤不肖交口称之而无毫发遗憾者，公一人而已。
——俞樾评彭玉麟