徐潮生：女性的身不由己与无望的相思

　　排名第十
　　容貌：★ ★ ★
　　武功：——
　　智商：★ ★ ★
　　情商：★ ★
　　怨妇指数：★ ★ ★
　　攻击力：——
　　徐潮生即是《书剑恩仇录》一书中陈家洛的母亲徐氏。
　　徐潮生与“红花会”前总舵主于万亭的爱情故事及悲剧，《书剑恩仇录》书中并未正面写到，只是侧面隐约叙述。我们知道了徐潮生和“红花会”前总舵主于万亭之间，有一段凄然伤感而且高尚动人的爱情故事。
　　徐潮生爱的是于万亭，而听从父母之命、媒妁之言嫁给了别人（陈阁老），作为女性，她是身不由己的。她无法选择，也无法抗争，她和于万亭一样，都忍受了现实惨淡的结局，认了宿命。
　　于万亭做出的牺牲，更是让人感动。他不仅忍受了爱人嫁给了别人的痛苦，还勇于做出奉献，居然易容乔装打扮隐藏起自己的真实身份，在陈家做起了长工，而且一做就是多年。于万亭这样做，一方面是可以暗中保护自己心中的爱人，另一方面也是为了慰借自己对心目中的爱人的相思之苦，能离自己的爱人近一点，能多看她一点，总是好的，总是聊胜于无的。
　　最后陈家洛的母亲徐氏将其最钟爱的儿子委托给于万亭，让陈家洛继承于万亭的反清大业，以寄托她的一份无望的相思。
　　十大怨妇上榜人物，陈家洛的母亲徐氏徐潮生，排名第十。