陈禹：坏蛋总是自作孽不可活

　　排名第八
　　武功：★ ★
　　智商：★ ★ ★
　　情商：★ ★
　　小恶指数：★ ★ ★
　　攻击力： ★ ★
　　依然是因贪欲而生罪恶，陈禹觊觎师叔太极拳九诀的秘密，乘其师叔病重之时下毒手。吕小妹请来赵半山主持公道，赵半山追缉陈禹，因此来到了商家堡。
　　对于大奸大恶之徒，金大侠往往不让主角亲自解决他们，只是废其武功饶其性命，但天网恢恢，疏而不漏，这些坏蛋总是自作孽不可活，自讨灭亡。众人最后已饶过陈禹，陈禹自己却不小心死于商老太铁厅烈火的机关之下，免去大家的麻烦。
　　十大小恶上榜人物中，陈禹排名第八。