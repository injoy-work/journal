凌霜华：人淡如菊清秀绝俗

　　排名第五
　　容貌：★ ★ ★ ★ ★
　　武功：——
　　智商：★ ★ ★
　　情商：★ ★ ★
　　悲情指数：★ ★ ★ ★
　　攻击力：——
　　丁典和凌霜华之间的爱情故事，最为动人，也是此书中一个突出的真善美的闪光点，在此书诸般人性的极度丑恶对照下，尤显其可贵和夺目之处。在菊花烂漫开放，清雅高洁的浪漫气氛中，两人因菊花而生出此生不悔的情缘，一见钟情，两情相悦，从此海枯石烂，此心不渝。
　　最为理想的爱情中，却有一个最大的反角在其中横刀夺爱，而这个反角竟是凌霜华的父亲凌退思。凌退思的蛇蝎心肠，写来真是让人心惊肉跳。虎毒不食子，但凌退思因贪欲的恶性膨胀而生出狠毒，竟将女儿作为无辜的牺牲品。假意将女儿许配给丁典，凌退思却暗中布下陷阱，将丁典下毒擒获下狱。凌霜华为表明对丁典的真情，自毁容貌，以绝了其父相逼之念。丁典心中的痛惜，他有高明武功，本可以轻易越狱得到自由，却依然甘愿在牢中关着，只为了每天要看凌霜华在窗口放的花。
　　丁典是豪放的江湖豪杰，凌霜华是个“人淡如菊”清秀绝俗的才女，英雄美人，悲剧感人。丁典对狄云道：“兄弟，你为女子所负，以致对于天下女子都不相信。”丁典给狄云上了一课，使只会认死理的狄云，也能理解了这世界的丰富和复杂，恢复了他内心中纯良朴素的一面。
　　凌霜花的悲情之旅却已走到了尽头。蔷薇花瓣飘零，凌霜华香消玉殒，丁典也再次中了凌退思布下的“金波甸花”之毒。
　　在十大悲情女上榜人物中，凌霜华排名第五。