金蛇郎君夏雪宜：没有出场的主角

　　《碧血剑》中除袁承志外，另一个很重要的人物是“金蛇郎君”夏雪宜。
　　夏雪宜的故事几乎贯穿全书，但夏雪宜却根本没有出场，因为书中一开始，他已经就是死人了。
　　金庸吸取外国文学作品的表现手法，第一次在武侠小说中运用倒叙和插叙的形式来展现波澜起伏的故事。
　　“金蛇郎君”虽然在书中并未出现过，但通过温仪与何红花两个女人的动情的叙述和追忆，他的情性、身世都一一地展现出来了。
　　武侠小说中倒叙和插叙的使用，使小说的表现形式更丰富，更能体现出起伏跌宕、波澜壮阔的场面和情节，增加了武侠小说的可读性、艺术性，深刻性和复杂性，而这还是新派武侠小说和旧派武侠小说的重要区别。
　　金蛇郎君夏雪宜，是本书中虽未出场却戏路很重的精彩人物，极为神秘诡异，也极具个性。袁承志无意中找到金蛇郎君秘藏尸骸和秘籍的洞穴，金蛇郎君的故事也自此逐渐展开。
　　金蛇郎君行事处处透着三分邪气，正如穆人清指出的“此人用心深刻，实非端士”。此人死后还要想着算计人，若发现他藏身洞穴之人稍有贪心，未能依照他留简的指示先葬其骸骨再开启宝盒，即难逃大难。袁承志的性格和郭靖、张无忌一路都是志诚君子，厚道老实，所以能遵其所嘱而行动，避过了危险。
　　金蛇郎君的秘密不仅是绝世武功秘笈，还有重宝之图，还有一个令人遐想不已的美丽的女子的名字，以及他留下的痛悔的诗意和浪漫的撩拨人心的叹息。
　　袁承志与温家诸人大战，却处处是为没有出场的主角金蛇郎君夏雪宜出彩，他自己一身从穆人清和木桑道人那儿学来的高明武功反倒退而居其次成了配角。而金蛇郎君的故事继续推演，居然温青青是其女儿，温青青的母亲温仪是其妻子，让读者大感兴趣。
　　金蛇郎君的奇情故事浮出水面，让人叹息和惊异。金蛇郎君不是一个正面人物，他古怪、多智、亦正亦邪，行事处处透着三分妖异的邪气，对于世界和人生的看法，他有偏颇的一面，但又非常深刻，他的内心有一种非常规的良心道德判断，是社会中的不公、罪恶和仇恨扭曲了他的心性，他纯良的本质被仇恨的毒质侵蚀和矇蔽了，他以暴易暴，用不公正对待不公正，用邪恶来报复邪恶，他的世界观和方法论已走火入魔。
　　这样的奇异才智之士，身上却另有一种邪恶的魅力，让人迷惑和怜惜，连袁承志都先入为主地在内心深处佩服他，不自觉地对他生出亲近的好感，也许正是这种奇异的魅力，使得本来是受害者的温仪，最终却死心塌地站在了仇人的那一边，原谅和接受了暴力侵犯的仇人。
　　金蛇郎君与温仪的故事，与《倚天屠龙记》中杨逍和纪晓芙的故事有相似之处，但前者的关系却更纯洁和感人，更缺少一种情欲的暖昧性，同样是男性暴力对女性的侵犯，金蛇郎君的态度更接近我们常规道德所能接纳的程度，也就是更为人性和合理。金蛇郎君的邪恶和仇恨，遇到了如天使般纯洁温良的温仪之时，他内心中善的一面被本能地激发出来，他深深埋藏起来的温柔和爱的天性再也不能人为用强地阻挡。他突然表现出来的对温仪的柔性和极大的耐心，非常的感人。他唱小曲给温仪听，用木头削成小玩具给温仪玩，拿出他妈妈绣花的红肚兜给温仪看，给温仪讲小时候父母、哥哥姐姐的事，表现出他极为人性的一面，极为细腻的感情，接近于诗人的善感的气质。他没有用强，只是耐心等待着温仪接受自己。他和温仪的爱情，比之杨逍和纪晓芙有着更多合理的解释和理由，与杨逍相比，他实是内心对女人有着更多的本能的尊重。
　　金蛇郎君故事的魅力，实在把袁承志、黄真、崔希敏与吕七、温家诸人的争斗比了下去，袁承志处处成了金蛇郎君的代言人，处处要为金蛇郎君出彩，他自己反而像个隐形人。
　　袁承志不论在《碧血剑》书中如何风光，我们还是总忘不了他身后的那个魅力神奇的天才的金蛇郎君，遥想他的天才风范。温仪最后被温家老四杀死，了结金蛇郎君一段孽缘。温仪临死前得知金蛇郎君对他的珍爱和无尽牵挂思念，含笑而死，总算是心中有所慰借。
　　焦公礼与闵子华的一场恩怨，是非曲直并不复杂，善恶自明，由此处生出的变故，正要让袁承志来一展身手，有所作为。然而无论如何表彰袁承志，总离不开金蛇郎君的身影，总让人觉得金蛇郎君才是真正的主角，金蛇郎君才最具吸引人的神秘魅力。
　　十大奇人上榜人物中，夏雪宜排名第三。