纪晓芙：非伦和败德的孽缘

　　排名第六
　　容貌：★ ★ ★ ★ ★
　　武功：★ ★ ★
　　智商：★ ★ ★
　　情商：★ ★
　　悲情指数：★ ★★ ★
　　攻击力：★ ★ ★
　　纪晓芙的故事，同样是一个奇特和晦涩难明的故事。明明是杨逍在纪晓芙不情愿的情况下，用暴力和胁逼强奸了纪晓芙，而最后纪晓芙却明言不悔，还给女儿取了不悔之名。
　　是处子对男人的性臣服，还是纪晓芙真的在被奸的过程中动了真情爱上了杨逍？这两种情况都不是我们的日常经验中所能想象的。即使是武侠小说，金大侠也写得如此惊人和深刻，令人有回味无穷的思考。
　　这是一段非伦和败德的孽缘，但纪晓芙以生命的宝贵再次证明了她真挚和纯洁的幻美。只有在殷梨亭的面前，她才有一种相负的歉意，她才想到爱情并不是私人的事，爱情还与社会和公众在某种程度上发生关联。爱情的实现，有时免不了要伤害他人。在十大悲情女上榜人物中，纪晓芙排名第六。