画家-事件

齐白石的画是大家公认的贵，一幅画几千万到9亿的高价画作 。

看过有几个鉴宝节目 。

就一个女的，故意打扮得很朴实无华的样子，说自己手上有一副齐白石的画作，专家鉴定过后，假的 。

然后大反转来了，齐白石的孙女在主持人的盛情邀请下上台来，说这画是真的，专家被打脸啦！

或者这个女的就自称自己是齐白石的孙女 。总之就是要打鉴宝专家的脸 。

看到这，可能你还不明白我要说什么 。可能会猜，我要证明这女人不是齐白石的孙女 ？

不是 。

~~~~~~~~~~~~~~~~~~~~~

不，我想说的是，齐白石93岁才走的，活到了1957年，是现代人 。

齐白石是木雕师，画家，书法家，篆刻家 。

齐白石的画，在他还没走以前，已经很值钱，很出名了。

齐白石孙女拿出来的那些画，一口咬死说是自己爷爷的画作，没有一张曾经在齐白石生前，死后几年那段时间展出过 。

印章是对的，因为齐白石的印章留给了后代。

纸张是对的，因为纸张是现代的纸张，这些纸齐家家里多得是。

墨水也是对的，因为墨水也是现代的。

也就是说，科学鉴定的方法，根本无法鉴定出画究竟是哪一年画出来的 。

鉴宝的专家只能根据齐白石办的画展一幅一幅的对出来 。对出来确实没有这幅画 。

但是齐白石的孙女们不认可这个方法啊。

齐白石的孙女们要把最终解释权这个大杀器牢牢掌握在自己手上 。

简单来说，就是自己编 。这画哪年她们爷爷画出来的，她们来说。哪怕那时候她们还没出生。

~~~~~~~~~~~~~~~~~~~~~~~~~~~

笔墨纸砚章都是齐白石遗留下来的 ，只要有个模仿齐白石画得好的，这几个孙女拿遗物里的章一盖，就硬说是齐白石真迹 ，任你专家研究齐白石画作再厉害，知道得再多，哪怕是齐白石亲传的徒弟都说不了她们 。

一幅画几亿，印钞机再快都赶不上她们几个盖章速度快 。

现在有十几幅画是找不到哪一年画的 ，没有展出过，是不是真的齐白石画的也不知道，只知道，上面盖的章是真的，墨水是真的，纸张也是真的 。印泥也是真的 。这些东西都是现代的。

所有真的东西都是能在齐白石走后保存下来的。

巧了，这十几张画里有不少是齐白石的孙女们主动站出来拍胸脯保证是真的 。

~~~~~~~~~~~~~~~~~~~~~~

你们现在明白我在说什么了吧 。

她们只要没钱花了，隔三差五的拿一幅画起来，啪一下盖个章说，这是我爷爷的真迹，我整理房间的时候发现的，原价九个亿，现在一亿卖给大家，我是齐白石孙女，我保证画是真的 。

毕竟也不是第一次这么干了。

现在十几幅画，以后可以几百幅画 。

别人家传家宝，一两个亿的画，她们自己盖个章就能卖这数。

你猜猜看，原来拥有齐白石生前就出售的画作的那些收藏家，会气成什么样子 ？

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

掌握“最终解释权”的名场面，有一个是孙悟空骗葫芦。

当年一根藤上结了两个葫芦，你们两个小妖怪手上的这个葫芦个头小是雌的，我手上这个个头大，是雄的 ，我的这个雄葫芦管着你们手上这个雌葫芦呢 。你这个雌葫芦顶多就是装人，我这个雄葫芦可以装天呢 。

谁说我爷爷当年就画了一张百虾图 ？！我是他孙女我怎么不知道。

同样的画，我家有九张呢 。我爷爷当年画了十张，卖了一张，还有九张放家里存着呢 。

这一张百虾图是1.2亿，那我这九张就10亿啦 。

~~~~~~~~~~~~~~~~~~~~~~~~~~

有人非得说我是被营销号影响，这事早就辟谣啦之类的。

拿了一个2013年2月，传家宝节目，那位叫齐慧娟（最小的孙女）的视频，说我造谣了，或者被营销号谣言影响了。

齐白石又不止一个孙女。

我一直写的是孙女们，因为当时我看的那几期节目，不是齐慧娟，是另外两个，或者说两个以上的孙女。一起走出来的。

第一个走出来的我印象较深，齐下巴短发中年妇女，略肥胖，有种洪金宝的气质在里面，这个中年妇女是姐姐一类的，一直都是她在发言 。

然后和另外一个妹妹或者两个相拥在一起，长相也接近。

而辟谣视频里的那个齐慧娟，眉毛那里有颗大痣，如果我看过，肯定会记得这么大的一个脸部特征 。但我刚好就没看过她 。我这个回答里，在我更新添加这段文字之前，6月18日下午16点之前都不曾写过齐慧娟这三个字。（是我这个回答）

用于辟谣的那个节目里，齐慧娟拿出来的是秋蝉图 。

而我看的那几期打脸鉴宝专家的节目里，是虾图 。

因为齐白石最擅长的就是画虾 。

这个辟谣视频的出现，就是为了修改别人的记忆，就是要把之前的那些脏事掩盖过去。

互联网总有人有记忆的 。

~~~~~~~~~~~~~~

另外说一句，如果非要说我造谣，齐家后人那么多，有钱有势，你大可以去通知她们。说不定念你有功送你一幅呢，起码几十万。我不恨你，还给你指条赚钱的路子 。

那个回答的贴图里，贴出来的是金运昌先生的辟谣。辟谣的是齐慧娟一个人的的那么一件事情。

我说的内容里，并没有说金运昌和齐慧娟。

我倒是想问问，金运昌可以辟谣关于自己的事情。

齐家的事情，金运昌可没资格辟谣吧？

为什么齐家这么多人，这么多年，这么大损失，受这么大委屈，不自己出来辟谣？

就那位说我说的是陈年老谣的，你很喜欢用抖音搜索辟谣视频吗？

你搜出来的究竟是非齐家的人发的辟谣视频，还是齐白石后人自己说的辟谣视频呢？

找不到吧。

我应该没那么大的网络势力把齐家后人的辟谣视频给全网删除吧。