# 小床快餐- 事件

最近上海开始流行家庭式小床经济，是良家妇女失业以后在自己家里开发自身资源发展经济。

这种模式的优点是安全，不怕人民公仆来个扫黄，再扫黄也不可能扫到人家家里去，没有人剥削，不像在会所、夜店之类的地方要交很高的抽成。

做这个生意的有夫妻档，丈夫给老婆招揽生意，站岗放哨，后来居然听说还有父女档，老父亲为女儿拉生意。

这让我想起了以前的”楼凤“，二十年前我曾经去江苏如皋，一下车就打了个摩滴，摩滴司机问我要不要去找“楼凤”，那些一般都是三十岁左右离了婚的女人，在家里营业。那个时候一般出差每天的住宿费用是两百左右，那些 “楼凤”也就每天收两百，不仅仅可以住她家，“楼凤”还提供一日三餐。

别的行业同行都是冤家，而这个行业同行是亲家，客户总有玩腻的时候，为了让客户有新的体验，”楼凤“之间互相介绍生意，同行成了最好的朋友。