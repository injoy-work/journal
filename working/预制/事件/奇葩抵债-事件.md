奇葩抵债

- 欠我钱，到处白嫖吃喝

# 打折买商品-事件

非标的资产其他人谁敢买？以前美国流行一个买大屏幕彩电的方法，就是买了立刻退货，不要说质量问题，就是后悔了，自己做个标记，以后每天去店里看，等在退货区看到就八折以下买回来。

因为彩电太大，店家和厂家再运走重新包装按新货卖成本太高，一开始都是本地处理，但开过封也不能撒谎。其他人哪里知道退货的原因，不打个对折都未必敢买。但退货的人自己知道没任何问题。

后来这种事多了，店家就自己承担成本，退回来的都运到别的大区去买。

我家以前做煤炭生意的，一些澡堂子买的煤炭比较多嘛，有一家就还不上，拿澡票抵债，每次我爹带我洗澡，人家还献殷勤，给我饮料啥的，我当时说谢谢，我爸直接说谢啥谢，都算在账上呢

我有个很要好的朋友，大学毕业之后参加工作，单位离他家十公里的样子，每天骑电动车上下班，差不多需要半个小时。

他参加工作之后买了一辆新电动车，但因为他爸工作单位更远，所以就将新电动车就给他爸用了。他自己骑他爸用过的那辆旧电动车。

但这辆旧电动车遇到下大雨的天气，会间歇性短路，开不起来。大半年时间一共遇到过三回，下着大暴雨，电瓶车突然熄火了，停在半路动不了，最后都是他老爸开着电动车来接他，拿根绳子绑在熄火的电动车上，父子俩就一前一后拖着回家。

他跟我吐槽说想买辆车，整个单位只有食堂阿姨和他每天骑电动车上下班。

14年五月份，他爸在地里干活，被毒蛇咬了。他爸骑上电动车往回赶，但毒素一上来，眼睛就看不清了，一下子摔进了路边的水沟里。

然后他爸赶忙给他二叔打电话，因为二叔家就在附近，想让他二叔赶过去救他，但因为眼睛看不清的关系，误打给了我朋友，在电话那头使劲喊他二叔的名字。他还笑着说老爸你打错电话了。他爸听到是他的声音才意识到打错了，连说打错了打错了，然后挂了电话。

当天中午他就接到他妈妈电话，说他爸可能要不行了，现在在去杭州的救护车上，让他赶紧买车票到杭州来。

他整个人都懵了，上午还给他打过电话，中午人就不行了。

他赶紧赶到车站，买了最近一班到杭州的汽车，他赶到医院已经是下午四点多了，他爸已经走了，连最后一面都没见上。

原来他爸挂了他电话之后，最终也没有拨通他二叔的电话，路边倒是有人看到他爸摔进了水沟里，但都以为他爸喝多了，最后是一个村干部过来查看，才拨的急救电话，但送到医院人已经昏迷了，没人知道咬他爸的是什么毒蛇，所以蛇毒血清也没有及时用上。当地人民医院只能让他们赶紧送杭州来。但送杭州后人也没有救回来。

过了几个月，我那个朋友就买车了，一辆[大众polo]九万不到。

他跟我说，有一天，下班路上又下大雨了，他的电动车又动不了了，这次没有爸爸来接他了，他一个人坐在熄火的电动车上放声嚎啕大哭，雨水夹着泪水，满脸都是。

他一直认为如果当时他有车，就能赶上见他爸最后一面。甚至认为他有车，他爸当时会给他打电话求助，他也能赶回家救他。车一度变成了他的心结。


