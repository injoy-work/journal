# 假玉-事件

说两万到农村只能活两年的可能没去过农村，我现在正在东北一个村里，看隔壁人家的院子大概一百平米以上，前些年说三千块买的，我所在这院子呢，墙外有两棵极其瘦弱的小枣树，一棵树下来的枣就有三大盆，说是有几十斤，每斤能卖四块五，这都是丝毫不用伺候，每年只要打一打然后捡起来就能得的钱，种地现在说不挣不挣的，一亩玉米也能打个上千斤，收购价一元，国家还补贴，政府盖个楼专门跟高铁合作，往外推荐鸭蛋等农副产品，闲人上山去捡柏子仁，回来敲一敲，一斤收购价也能卖十二，村里为防止坑农，还修了个地秤，卖牛卖粮都可以拉来称。隐居人总要烧柴做饭，可柴四处都是，秋天树杈子捡一个月够烧一冬天，根本不用买，喜欢液化气也可以灌，本地穷困户甚至是免费的。吃菜选个二十平米的荒地块，你整修一下，划分出池子，扔点菜籽，生菜十棵，韭菜两垄，小白菜萝卜菜两畦，吃不了的烂西红柿角瓜埋地里，长出来的自己吃不了还能待客，会搭架子豆角葡萄挂的哪都是，根本吃不过来。对，还有蝎子，这的人没事上山拿镊子夹蝎子，收购价一斤蝎子四百五，顺手采蘑菇，晒干一斤一百二三，也就城里人挨训受气的加班牛马，但凡想得开跑出来活得自在不难。

翡翠在明朝被称为”假玉”，价格不足和田玉5%，直到清朝，满族权贵不懂，被翡翠配饰忽悠了，才建立了共识，支撑了后续的价格。

不要嘲笑NFT和数字资产，这些东西本质上也是”玉石”。

共识消减趋势下，流动性越来越差，而流动性差的资产必然大幅度贬值，最终引发踩踏出货。

勿谓言之不预也。




# 自杀

Q 为什么李自成找崇祯花了4天？煤山也不大啊？
知乎·21个回答·89个关注>项天鹰
+关注 2343人赞同了该回答
不是四天，是两天，崇祯三月十九凌晨上吊，午时李自成°进城，三月廿一崇祯被发现。
崇祯煤山上吊这事实在太难猜了。在此之前，崇祯表现出了强烈的求生欲望，在杀妻女、弃儿子之后，他“脱黄巾，取承恩及韩登贵大帽衣靴着之”，化装成太监想逃出京城，怎么看都是个能屈能屈的人啊。
崇祯只是由于逃命技术太差，再加上大明守军过于离谱，才没逃出去。有的城门压根没人值班，崇祯打不开锁；有的管城门的勋贵躲在家里，崇祯到他家里找他开城门，他让保安把崇祯赶走；有的城门干脆以为崇祯是顺军的内应，朝他开炮。
出东华门，内监守城，疑有内变，施矢石相向。出东华门，至齐化门，内监守门者，疑有内变，将炮矢相向，不得南奔。
时成国公朱纯臣守齐化门，因至其第，阍人辞焉，上太息而去。
走正阳门，将夺门出。守城军疑为奸细，弓矢下射。守门太监施炮向内。急答曰：“皇上也！”炮亦无子，弗害。
搞笑的是，救了崇祯一命的恰恰是顺军的恐吓，因为守城明军不敢真的打死顺军，所以才不装炮弹，只放空炮吓人。
丙午，寇攻城，炮声不绝，流矢雨集。仰语守兵曰：“亟开门，否且屠矣！”守者惧，空炮向外，不实铅子，徒以硝焰鸣之，犹挥手示贼，贼稍退，炮乃发。
贼驱居民负木石，填濠急攻。我发万人敌大炮，误伤数十人，守者惊溃，尽传城陷，合城号哭奔窜。明军一不小心用了真炮弹，误伤了顺军拆工事的民夫，立刻吓得自己溃散了。
守军这么糊弄也是理所当然的，因为他们大部分压根就不是兵，而是京城的老百姓。那些正经的官军被欠了五个月的工资，压根不想打。而崇祯让他们参战的待遇是“饷久阙，又无炊具，人给百钱，市饭为餐”。
工资不补发，就给一百块钱，还不管饭，任务是跟顺军玩命，谁去谁煞笔。何况官军士兵很多本来就是吃空饷的，压根没训练过。
但是上面催得紧，官军就把这个项目转包给了京城的穷苦百姓。
京军五月无粮，一时驱守，率多不至；又守陴军，皆贵近家，诡名冒粮，临时倩穷人代役，仅给黄钱百文。
官军士兵还算厚道，把崇祯给的一百文原封不动转给老百姓了。
这些临时工当然舍不得买饭，于是让家里送饭。然而由于城防组织一片混乱，送饭的家人根本找不到他们在哪：“守垛之兵，饥不得食。或母或子，携粥至城下狂呼，不知守之所在。”
守军能放空炮吓唬一下顺军，已经算对崇祯仁至义尽了。
在这种情况下，李自成来到皇宫，看见只有崇祯的三个儿子和被崇祯砍伤的公主向自己投降，第一反应就是，崇祯肯定是让那些太监吸引注意力，混在守城的老百姓里跑了。
大臣们的看法也差不多，新投降的黎志升急于表忠心，就跳出来说：“此必匿民间，非重赏严诛不可得。今日大事，不可忽也。”于是，顺军一面在城内搜查，一面派人出城向南追捕崇祯，还悬赏黄金一千两通缉崇祯。
而且此时顺军还有更重要的事情做。十九日下午这半天，光是安排顺军各部的驻地就忙得要死，还没做完，第二天上午继续安排。
二十日，李自成通知全城的官员明天来开会，失踪的不止崇祯一个人，很多官员也藏匿民间，顺军要把他们一个个都揪出来。牛金星°、宋企郊等文官忙
着抢占明朝各部门的办公室，如果动作慢了，里面
的档案恐怕都得被老百姓搬走当废纸卖了。
这一天顺朝最主要的工作是招降密云、通州、天津、涿州等地的明军，还要派人联系吴三桂°。其中最急迫的任务是拿下通州，这里是漕运枢纽、粮仓重地，虽然实际上守卫这里的明朝户部侍郎党崇雅“见了顺军的几个骑兵就直接投降了，但顺军做预案的时候也得按照他不投降来做战备，闲不下来。有这么多正事要做，也没太顾得上认真找崇祯。破城之初，城内秩序很混乱，溃散的明军满城乱窜，顺军自己的部队因为最近扩招太快也不太好控制，乱兵和地痞流氓趁机抢劫。赵应元等人带着军法队在城内弹压，逮住就大卸八块。这乱糟糟的，谁能想到去煤山看一眼。
顺朝对崇祯的追捕，都是以崇祯还活着、想逃命为前提的。煤山在城市最中间，一没有食物二没有水，按常理来说，崇祯绝不可能跑到那里去。
廿一日，顺朝最要紧的工作就是整编投降的三千多明朝官员。等接见完这些人，李自成也有点弄不清了，附近的重镇除了被刘芳亮包围的保定都投降了，可哪里都没有崇祯的下落。
这个时候，顺朝还有两件事在策划中。一件是要裁撤太监，使太监总数不超过一千人；另一件是李自成要在煤山搞军事演习，观将士骑射。
于是，立刻就有想留任的太监积极表现，来煤山打扫卫生，结果意外发现了崇祯的御马。太监们赶紧呼叫顺军，顺藤摸瓜这么一找，崇祯和王承恩°的尸体就被顺军士兵李才发现了。





