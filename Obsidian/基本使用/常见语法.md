 
## 1. 任务类型示例
- [ ] 测试完成时间 16:00-18:00
- [/] incomplete
- [x] done
- [-] canceled
- [>] forwarded
- [<] scheduling
- [?] question
- [!] important
- [*] star
- ["] quote
- [l] location
- [b] bookmark
- [i] information
- [S] savings
- [I] idea
- [p] pros
- [c] cons
- [f] fire
- [k] key
- [w] win
- [u] up
- [d] down

就吃过一次核桃树枝烤的牛肉。核桃树枝是从后面小山上捡的。

## 2. 目前支持的样式列表
### 2.1. 提示框类型
> [!note]
> Here's a callout block.
> It supports **markdown** and [[Internal link|wikilinks]].

> [!abstract]

>[!todo]

> [!info]

> [!tip]

> [!success]

> [!question]

> [!warning]

> [!failure]

> [!danger]

> [!bug]

> [!example]

> [!quote]

### 2.2 提示框的各种用法

1. 可以没有内容直接显示标题
> [!TIP] Callouts can have custom titles, which also supports **markdown**!

2. 折叠提示框
> [!FAQ]- Are callouts foldable?
> Yes! In a foldable callout, the contents are hidden until it is expanded.

3. 自定义提示框
可以通过css设置my-callout-type 的样式
```css
.callout[data-callout="my-callout-type"] {
    --callout-color: 0, 0, 0;
    --callout-icon: icon-id;
    --callout-icon: '<svg>...custom svg...</svg>';
}
```

### 2.3. 

> [!error|leftght-small] 浮動到右側
>
> 小視窗，靠右


## 3. 代码域 embed
### 3.1. 测试callouts

> [!note]+ 测试
> dlfjl
> 士大夫街里街坊了

> [!tip]+ 测试
> dlfjl
> 士大夫街里街坊了

> [!quote]+ **原文摘录**
>
>>长颈鹿的头部距离地面约5米，心脏距离地面约3米，因此心脏需要很高的血压才能把血液输送到2米之上的头部。我们比较了哺乳类动物的血压并得到如下结果：人类120mmHg，犬类110mHg，牛160mmHg，猫170mmHg，而长颈鹿的血压是260mmHg，明显高于其他动物。

## 4. 多行分列语法MCL Multi Column.css
## 5. 多栏样式（Multi Column）

> [!tip] Callouts类型  
> 1. 双栏：> [!multi-column] 【快捷输入：co-mulit，会自动帮你构建双栏空位】
> 2. 三栏：> [!multi-column] 【快捷输入：co-tri，会自动帮你构建三栏空位】
> 3. 理论上讲只要宽度足够，你可以便问N多栏；
> 4. 隐藏标题列：> [!blank-container]
> 5. 快捷输入依赖：需要开启【various-complements】插件
> 6. 支持这个样式需要在：OB > 外观 > 【MCL Multi Column.css】（状态变为打开）
> 
> [!tip] 使用方法  
> 1. 栏位间用一个 > 分隔  
> 2. 每个Callout区块多增加一个 >  
> 3. 栏位数由2到N，只要屏幕宽度足夠，会自动分配栏宽  
> 4. **可使用Style Settings 插件设定**

### 2.1. 两栏示例

> [!multi-column]
>
>> [!note]+ 待办事项
>>
>>- your notes or lists here. using markdown formatting
>>- your notes or lists here. using markdown formatting
>>- your notes or lists here. using markdown formatting
>
>> [!warning|right-small]+ 进行中的事项
>>
>> your notes or lists here. using markdown formatting

### 2.2. 三栏示例

> [!multi-column]
>
>> [!note]+ 待办事项
>>
>> your notes or lists here. using markdown formatting
>
>> [!warning]+ 进行中的事项
>>
>> your notes or lists here. using markdown formatting
>
>> [!success]+ 已完成事项
>>
>> your notes or lists here. using markdown formatting

### 2.3. 四栏示例

> [!multi-column]
>
>> [!note]+ 待办事项
>>
>> your notes or lists here. using markdown formatting
>
>> [!warning]+ 进行中的事项
>>
>>- your notes or lists here. using markdown formatting
>>- your notes or lists here. using markdown formatting
>>- your notes or lists here. using markdown formatting
>
>> [!success]+ 已完成事项
>>
>> your notes or lists here. using markdown formatting
>
>> [!info]+ 说明
>>
>> your notes or lists here. using markdown formatting


## 6. MCL控制
本库集成的【MCL Multi Column.css】也通过 【style setting】插件实现了图形化设置。
位置：style settings > Modular CSS Layout - Multi Column
这里已经对部分设置进行了汉化，如果你的OB设置了中文，那么会自动展示已经翻译的内容。
