
## 案例分享

以下分享案例排序，按照网友昵称字母排序

*   @Adrius
    
> I tried using Canvas for creating a milestone plan, and I am very happy with the result so far. The real gem is how I can open each of these notes to get more details and connected notes I don't want to display. Good bye, Excel milestone plans.

用作对重要事件的记录

![图片](https://image.cubox.pro/article/2022122709172217261/96709.jpg)

*   @Alisha
    

> My first canvas. Visual literature notes, anybody? 😍

可视化的文献阅读笔记

![图片](https://image.cubox.pro/article/2022122709172231274/21121.jpg)

*   @Boninall
    

> A canvas for translating obsidian roundup into Chinese, time flows.

Bon佬用canvas分块展示了Ob周报

![图片](https://image.cubox.pro/article/2022122709172299074/47092.jpg)

*   @Cal
    

> Ok cool will do Also playing around with a kanban style pipleine for literature reading

用Canvas做了一个文献阅读看板，很有想法👍![图片](https://image.cubox.pro/article/2022122709172275749/25918.jpg)

*   @Carboxyl
    

> a little study guide i made for myself hehehe 知识总结，清晰明了

![图片](https://image.cubox.pro/article/2022122709172294539/87474.jpg)

*   @ConcernedSquirrel
    

> Love the new Canvas ❤️ It makes combining different points of a Topic so easy - I used it now the first time for all my Information of the Consulting-lectures I had the last weeks. Like that you can follow a path for different Informations so quick and easy. But as someone already mentioned yesterday (or 2 days ago cant remember) at some point it gets a bit laggy and you need to reopen the canvas - which also affected a newly opened file - after a restart it was fine again 🙂

白板的一个好处就是你可以将与某个话题相关的所有points呈现在一张纸上，通过连线、颜色和标注厘清其中的逻辑。

![图片](https://image.cubox.pro/article/2022122709172279925/79060.jpg)

*   @Damian Korcz
    

> My WIP Theme Test canvas. A collection of all my usual test files and Canvas features (has all the colour variations for nodes, groups and arrows).

用canvas写了一个主题测试说明

![图片](https://image.cubox.pro/article/2022122709172223796/44473.jpg)

*   @Embrace Chaos
    

> Mapped out my house with links to the notes on maintenance, repairs, upgrades, etc.

什么？！用canvas卡片设计了房子的布局图？？？这创意👍

![图片](https://image.cubox.pro/article/2022122709172279306/38175.jpg)

*   @erik.aybar
    

> I've found a handful of different uses for Canvas by now but this year in review exercise was illuminating for me showing how flexible and useful it is. This canvas gives me a high level view across months and quarters divided up by "areas" and memories. This is just one area within the larger canvas. I've got plenty more to add in, but I'm impressed at how it holds up with many cards, overlapping groups, embedded notes and block references (even one embedded canvas), and URL embeds (all of the photos across the top). I love that you can just paste a URL and it automatically embeds ready to position and resize which made creating the memories section nice and quick! Excellent work on this Obsidian team! 👏❤️

用canvas做了一整年的复盘

![图片](https://image.cubox.pro/article/2022122709172236227/84974.jpg)

*   @lunaris13
    

> Omg, thank you thank you thank you developers! Now it's so much easier to see the different groups of animal classification with canvas grouping! It's so much clearer. I think you are saving my grade right about now 😂 Edit: this is a work in progress; already made many changes, but they are so easy to make, which is perfect!

可视化专业知识笔记

![图片](https://image.cubox.pro/article/2022122709172267151/89579.jpg)

再来一图

> Now it's finished. This was the class material for my first Computer Science class. It was heavily based on the textbook given, Starting Out With Python by Tony Gaddis. It's been fun learning how to code, finally. (I grew up in a house full of computers, with family members that always talked about computers, technology, or IT stuff. Now I seemed to have caught up. :D)

![图片](https://image.cubox.pro/article/2022122709172223124/58292.jpg)

*   @Ian Hayes
    

> Groups are so helpful! I'm collecting feedback on a short story from my critique group, and I've been able to group individual comments by theme and difficulty rating, each with their own block references back to the original text. I feel like this plugin gets better every day!

canvas的group功能

![图片](https://image.cubox.pro/article/2022122709172257840/38867.jpg)

*   @jgoodman
    

> Personal research project I'm working on, so nice to be able to throw everything down onto a workspace like this. My WIP "research" about How to find work we love is evolving and I'm loving canvas even more!! I used to not really find mind maps helpful, but to be able to have such easy access to my obsidian notes in here is next level. Not sure if I'd have this sort of clarity on the project without it. 继续用canvas做学术研究

![图片](https://image.cubox.pro/article/2022122709172253551/61526.jpg)

*   @keara
    

> outlining a research article 🙂 mapped out each section with relevant concepts and evidence. canvas is truly everything I've been needing for my work!

又一用canvas做文献阅读的

![图片](https://image.cubox.pro/article/2022122709172216532/81732.jpg)

*   @Klaas
    

> I was intrigued a long time by the question of how money comes into being. I could not believe that it was just a question of writing $ bills (or whatever currency), and that was it. So, I spent time reading up about it, making notes in Obsidian as I went along. Obviously, everything was explained in words, which did not make it any easier. Eventually I got to a point where I think I had the main points, and in order to give myself an overview I tried to incorporate those points in a table. Then, just as I had finished the table, Canvas came out. Wow, what a game changer!! I made my 1st canvas, which proved the old adage: “a picture is worth a 1000 words”. So, I thought I'd share my 1st canvas with you. It is based on the Federal Reserve System and commercial banks in the U.S., but it is probably applicable to most other countries too. For those who are interested in the actual content, do not hesitate to criticize, either positively or negatively. Spoiler: yes, it effectively amounts to printing money, but in a very convoluted way.

![图片](https://image.cubox.pro/article/2022122709172250830/30448.jpg)

*   @linkDDD
    
> Knowledge City 学到一个新词

![图片](https://image.cubox.pro/article/2022122709172230244/61708.jpg)

![图片](https://image.cubox.pro/article/2022122709172298529/41174.jpg)

*   @MinecraftMike16
    

> Storyboarding becoming easy... I know this isn't the most impressive graph, but this is going to help me keep my novel ideas in order before they multiply out of control

![图片](https://image.cubox.pro/article/2022122709172249558/70575.jpg)

*   @nematoad
    

> Getting started on a canvas about the protein complex family i study 🧬

![图片](https://image.cubox.pro/article/2022122709172227921/40965.jpg)

*   @pewu
    

> Canvas for roadmaps. This is way too amazing. Each card contains a deep dive into each feature.

![图片](https://image.cubox.pro/article/2022122709172240184/18987.jpg)

*   ## @RyanC
    

> Spent the morning drafting a 1-pager about an important initiative. Canvas has been an absolute delight in helping me do this! From the easy markdown formatting in cards with quotes or todo checkboxes to the references to existing vault content. I didn't expect to value or enjoy this as much as I am!

![图片](https://image.cubox.pro/article/2022122709172269377/77670.jpg)

*   ## @smoosh
    

> i have mocked up the "lifeboard" concept that i am using in my personal vault, using the role > aspiration > goal > project > task through-line to make sure my projects are aligning with my aspirations

![图片](https://image.cubox.pro/article/2022122709172288144/44293.jpg)

*   @stēla
    

> I just created a canvas that explains the architecture of my obsidian vault

![图片](https://image.cubox.pro/article/2022122709172263108/32626.jpg)

*   @vxxv
    

> First quick mind map with Obsidian Canvas, some much potential here

思维导图

![图片](https://image.cubox.pro/article/2022122709172277372/22427.jpg)

> You can add Database Folder embed as well as Data view queries which than you can link to objects// I've created Tasks

![图片](https://image.cubox.pro/article/2022122709172230932/78726.jpg)

> Another Idea of what you can do with Canvas, You can build a fully functional dashboard with links, pages images etc

![图片](https://image.cubox.pro/article/2022122709172242473/72308.jpg)

> Dashboard Template with working links to

这个有点儿酷，模板我试了，打不开

![图片](https://image.cubox.pro/article/2022122709172216617/28190.jpg)

> You can also use Admonition Plugin to embedded in to block, that you have a nice separation between multiple sections or even sections from different notes in one canvas

![图片](https://image.cubox.pro/article/2022122709172259454/35135.jpg)

分享完毕！

如果你有更多更好的Ob白板使用案例，欢迎分享啊，一起交流学习！

本人在Ob中文社区开了一个关于Ob白板使用案例的帖子，欢迎各位分享哈

帖子链接如下👉https://forum-zh.obsidian.md/t/topic/13721

![图片](https://image.cubox.pro/article/2022122709172272312/94937.jpg)
